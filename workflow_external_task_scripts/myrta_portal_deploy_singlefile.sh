#!/bin/ksh

#Author: Andrew Kyle / andrew.kyle@au.fujitsu.com
#Date  : 07/06/2012

# Modified by Lee Borkman 2013 to deploy a single file

##
## Script Requirements:
## --------------------
## Setup and script replication of content from ROCK2 to destination servers and any association work required 
## (eg directory setup, authentication, network, firewall changes etc).
##
## The replication must be mirror copy of the content. ie exact copy of content (files and subdirectories) allowing
## new, modify and delete of content.
##
## Provide a command (write replication script) that can be run by tsadmin from the command-line of ROCK2:
## eg USAGE: 
## myrta_portal_deploy SOURCE_FILEectory destination_server_and_directory1 destination_server_and_directory2 .
## (OpenDeploy will trigger the script) 
## 
## Return Success status 0 when:
## * EUT successfully replicated, else retry 2 more times (with short delay) and if still failure return error 
##   string with details.
## * BOTH UAT and SIT replicated successfully, else retry 2 more times (with delay) and if still failure return 
##   error string with details.
## * ALL LOAD and Production replication successfully, else retry 2 more times (with delay) and if still failure 
##   return error string with details. 
##


##VARIBLES
SCRIPT=`basename $0`
USAGE="Usage: $SCRIPT [-p CHMOD_PERMS] [src_dir] [server1:/dest/dir1] [server2:/dest/dir2] [...]"
HOST=`hostname|cut -d. -f1`
LOG=/var/tmp/cms/logs/$SCRIPT.log.`date +%Y%m%d`
FAIL_SEV=2
DEST_USER=soatssync
#SRC_BASE_DIR=/default2/main 
SRC_BASE_DIR=""
SYNC_ATTEMPTS=3
DELAY_SECS=30
DEF_CHMOD="u=rwx,go=rx"


##FUNCTIONS
Log(){
   echo `date +%H:%M:%S" "%d/%m/%Y`": $1" >> $LOG
}

SendAlert() {
        /usr/local/bin/emsg1 -n nova -p 2345 -w icecream -s $FAIL_SEV -t -1 -c INET -m "$SCRIPT sync failure from $HOST to $1: View the log file \"$LOG\" for more details "
}

SyncFiles(){
   #PATH=$PATH:/usr/local/bin
   #export PATH
   #COMMAND="/usr/local/bin/rsync -ave /usr/local/bin/ssh --delete $1 $2"
   COMMAND="/usr/local/bin/rsync -ave /usr/bin/ssh $CHMOD --rsync-path=/usr/local/bin/rsync --delete $1 $2"
   Log "Starting rsync using the command \"$COMMAND\"" 
   $COMMAND >> $LOG 2>&1
   return $?
}
  

##################
##	MAIN	##
##################
if [ $# = 0 ]
then
   echo "$USAGE"
   exit 1
fi
if [ $1 = "-p" ]
then
   CHMOD="--chmod=$2"
   shift 
   shift
else
   CHMOD="--chmod=$DEF_CHMOD"
fi

#Initialise the log for the start of this run
echo "#################################################################################################" >> $LOG
Log "$SCRIPT started by `id` on `date`" 

SOURCE_FILE=$1
if [ ! -f $SRC_BASE_DIR/$SOURCE_FILE ]
then
   Log "Attempted to run: \"$0 $*\""
   ERRMSG="Error: source file \"$SRC_BASE_DIR/$SOURCE_FILE\" does not exist" 
   Log "$ERRMSG"
   echo $ERRMSG
   exit 1
fi

shift
ERR=0
RC=0
for destination in $*
do
   count=1
   while [ $count -le $SYNC_ATTEMPTS ]
   do
      SyncFiles $SRC_BASE_DIR/$SOURCE_FILE $DEST_USER@$destination
      ERR=$?
      if [ $ERR = 0 ]
      then
         #Worked - so do no more syncs to this destination
         count=`expr $count + $SYNC_ATTEMPTS`
         Log "Successful rsync" 
      else
         #FAILED 
         #Was this the last attempt?
         if [ $count -eq $SYNC_ATTEMPTS ]
         then
            #Was last attempt so send error out
            Log "Failed rsync - sending an alert" 
            SendAlert $destination 
            RC=$ERR
         else
            Log "Failed rsync - will retry" 
            sleep $DELAY_SECS
         fi
         count=`expr $count + 1`
      fi
   done
done
exit $RC