#!/opt/local/sys/iw-home/TeamSite/iw-perl/bin/iwperl -w

use strict;

my $TMP = "/default4/main/ext/WORKAREA/ombudsman/html/ombudsman";

my @empty = ();
my @list = qw(stylesheets
scripts
monitor
helpmap
templates
cgi-bin
images
pagetemplates
search
registry/10853
registry/3509
registry/other_photocards
registry/3508
registry/3510
registry/maritime
registry/tolling
registry/11061
registry/121
registry/121/index.html
registry/index.html
registry/index_main.html
registry/index_b.html
registry/index_c.html
registry/index_d.html
registry/index_e.html
registry/index_f.html
registry/index_g.html
registry/index_h.html
registry/index_i.html
registry/index_j.html
registry/index_k.html
registry/index_l.html
registry/index_m.html
registry/index_n.html
registry/index_o.html
registry/index_p.html
registry/index_q.html
registry/index_r.html
registry/index_s.html
registry/index_t.html
registry/index_u.html
registry/index_v.html
registry/index_w.html
registry/index_x.html
registry/index_y.html
registry/index_z.html);

`/usr/bin/chmod -R 777 $TMP`;
#`/usr/bin/rm -rf $TMP/*`;
for (@list) {
	warn "CREATE: $_\n";
	if (/\//) {
		my $dir = $_;
		$dir =~ s!/.*?$!!;
		`/usr/bin/mkdir -p $TMP/$dir`;
		`/usr/bin/chmod 777 $TMP/$dir`;
	}
	#`/usr/bin/rm -rf $TMP/$_`;
	if (/\./) {
		`/usr/bin/cp -p /default3/main/deploy.frontlinehelp.rta.nsw.gov.au/STAGING/html/$_ $TMP/$_`;
	} else {
		`/usr/local/bin/rsync -av --delete /default3/main/deploy.frontlinehelp.rta.nsw.gov.au/STAGING/html/$_/* $TMP/$_`;
	}
}
`/usr/bin/chmod -R 777 $TMP`;

#exit;

########################



chomp (my @files = `/usr/bin/find $TMP -name "*.html" -o -name "*.css"`);

for (@files) {
	warn "$_\n";
	chomp (my $html = `/usr/bin/cat $_`);
	$html = process($html, $_);
	open HTML, ">$_";
	print HTML $html;
	close HTML;
	
	my $staging = $_;
	$staging =~ s!/WORKAREA/ombudsman/!/STAGING/!;
	`/usr/bin/diff "$_" "$staging" || /opt/local/sys/iw-home/TeamSite/bin/iwsubmit -w -r "$_" updated`;
}


sub process {
	(local $_, my $file) = @_;
	
	# Handle CSS first...
	if ($file =~ /css$/) {
		# references in css to sub-directory
		s!(url\s*\(['"]?/)!$1ombudsman/!gis;
		
		# tweak some colours to distinguish
		s!cc6600!ff3300!gis;
		
		return $_;
	}
	
	# ... then do the HTML files...
	
	# remove unwanted LH menu items
	s!<tr>\s*<td[^>]*><img[^>]*></td>\s*<td><a[^>]*>Online Services</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*></td>\s*<td><a[^>]*>Agencies, GACs ..... Govt Services</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*></td>\s*<td><a[^>]*>Quick reference</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*></td>\s*<td><a[^>]*>General administration</a></td>\s*</tr>!!gis;
	
	s!<tr>\s*<td[^>]*><img[^>]*>\s*</td>\s*<td[^>]*><a[^>]*>News</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*>\s*</td>\s*<td[^>]*><a[^>]*>Bookmarks</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*>\s*</td>\s*<td[^>]*><a[^>]*>e-Learning</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*>\s*</td>\s*<td[^>]*><a[^>]*>Training</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*>\s*</td>\s*<td[^>]*><a[^>]*>Intranet</a></td>\s*</tr>!!gis;
	s!<tr>\s*<td[^>]*><img[^>]*>\s*</td>\s*<td[^>]*><a[^>]*>Website</a></td>\s*</tr>!!gis;
	
	#remove unwanted top utility menu items
	s!<a href="/cgi-bin/feedback.cgi">Feedback</a> \|!!gis;
	s!<a href="/registry/2401/index.html">DRIVES screens</a> \|!!gis;
	
	# remove search form
	s!<form[^>]*"advancedSearchBoxForm"[^>]*>\s*(<input[^>]*>\s*)+<table[^>]*>\s*<tr>\s*<td[^>]*><input[^>]*></td>\s*<td[^>]*><input[^>]*></td>\s*</tr>\s*</table>\s*<table[^>]*><tr><td[^>]*>\s*</td>\s*</tr>\s*</table>\s*(<table.*?advancedSearch:aglscategory.*?</table>\s*)?</form>!!gis;

	# remove Advanced Search and Add Favourites links
	s!<td[^>]*><a[^>]*><img[^>]*></a>\s*<a[^>]*>Advanced Search</a>\s*.\s*<a[^>]*>Add page to My Favourites</a></td>!!gis;
	
	if ($file =~ m!/registry/index.html!) {
		# Remove Quick Links from Home
		s!<h1>Quick Links</h1>\s*<ul class="quicklinks">.*?</ul>!!gis;
		
		# Remove Favourites from Home Page
		s!<div id="user_favs">\s*<iframe.*?</iframe>\s*</div>\s*<div id="reg_links".*?<iframe.*?</iframe>\s*</div>!!gis;
		
		# remove News from Home Page
		s!<table[^>]*class="news">\s*<tr[^>]*>\s*<td[^>]*><h1>News</h1>.*?</table>!!gis;
		
		# remove feature box from Home Page
		s!<div[^>]*class="featureBox">.*?</div>!!gis;
	}
	
	if ($file =~ m!registry/(3508|11061)/index.html!) {
		#Remove a couple of How Do I? sections
		s!<h2>How do I\?</h2><table.*?</table>!!gis;
	}
	
	if ($file =~ m!registry/maritime/index.html!) {
		# Remove some items from the Maritime page
		s!<h3><strong>GLS OneGov<br /></strong></h3>\s*<ul>\s*<li><a href=".*?registry/maritime/gls_log_in/index.html">Using GLS OneGov</a>\s*</li>\s*</ul>\s*<ul>\s*</ul>!!gis;
		s!<h3><strong>GLS help and support:</strong></h3>.*?</ul>!!gis;
	}
	
	# disable links outside the sub-set
	s!href=(['"]?/registry/(online_services|agencies_gacs_govt_services|2401|5123|10856|12369|bookmarks|training|education|snsw))!href_disabled=$1!gis;

	# disable links to external sites
	s!href=(['"]?https?://)!href_disabled=$1!gis;

	# basic references to sub-directory
	s!((background|href|src)\s*=\s*['"]?/)!$1ombudsman/!gis;
		
	# Mod the page titles to show this is the ombudsman sub-set
	s!</title>! - Ombudsman edition</title>!gis;
	
	# Remove grey banner
	s!<table[^>]*>\s*<tr class="hide_in_print">\s*<td[^>]*class="secondbanner">.*?</table>!!is;
	
	return $_;
}