#!/opt/local/sys/iw-home/TeamSite/iw-perl/bin/iwperl

# define ALL server groups
our %serverGroups;

$serverGroups{"EUT-CONTENT"}	= [qw(EUT-CONTENT)];
$serverGroups{"REVIEW"}			= [qw(REVIEW1 REVIEW2)];
$serverGroups{"PROD"} 			= [qw(PROD1 PROD2)];
$serverGroups{"EUT-DEV"} 		= [qw(EUT-DEV1 EUT-DEV2a EUT-DEV2c)];
$serverGroups{"ST"} 			= [qw(ST1 ST2)];
$serverGroups{"LT"} 			= [qw()];
$serverGroups{"EUT2"} 			= [qw(EUT2)];
$serverGroups{"SAT2"} 			= [qw(SAT2)];
$serverGroups{"STGLT"} 			= [qw(STGLT1 STGLT2)];
$serverGroups{"STGAT"} 			= [qw(STGAT)];
$serverGroups{"EUT3"} 			= [qw(EUT3)];
$serverGroups{"SAT3"} 			= [qw(SAT3)];
$serverGroups{"EUT1"} 			= [qw(EUT1)];
$serverGroups{"SAT1"} 			= [qw(SAT1)];

$serverGroups{"EUT-CONTENT-T4"} = [qw(EUT-CONTENT-T4)];
$serverGroups{"REVIEW-T4"} 		= [qw()];
$serverGroups{"PROD-T4"} 		= [qw(PROD1-T4 PROD2-T4)];
$serverGroups{"EUT-DEV-T4"} 	= [qw()];
$serverGroups{"ST-T4"} 			= [qw()];
$serverGroups{"LT-T4"} 			= [qw()];

$serverGroups{"WEB-UNIT-TEST"} 	= [qw(WEBTEST1 WEBTEST2)];


# define ALL portal servers locations and docroots
our %servers;

$servers{"EUT-CONTENT"} = "soanp11z1:/opt/ibm/HTTPServerP7/htdocs/wcm";
$servers{"REVIEW1"} 	= "intltap1z5:/opt/ibm/HTTPServer/htdocs";
$servers{"REVIEW2"} 	= "soanp21z4:/opt/ibm/HTTPServer/htdocs";
$servers{"PROD1"} 		= "soapd11z4:/opt/ibm/HTTPServer/htdocs";
$servers{"PROD2"} 		= "soapd21z4:/opt/ibm/HTTPServer/htdocs";
$servers{"EUT-DEV1"} 	= "soanp11z1:/opt/ibm/HTTPServerP7/htdocs";
$servers{"EUT-DEV2a"} 	= "soanpp01z1:/opt/ibm/HTTPServer1/htdocs";
$servers{"EUT-DEV2b"} 	= "";
$servers{"EUT-DEV2c"} 	= "soanpp01z1:/opt/ibm/HTTPServer3/htdocs";
$servers{"ST1"} 		= "soanp11z2:/opt/ibm/HTTPServer/htdocs";
$servers{"ST2"} 		= "soanp21z1:/opt/ibm/HTTPServer/htdocs";
$servers{"EUT2"}		= "soanpp01z1:/opt/ibm/HTTPServer2/htdocs";
$servers{"SAT2"}		= "soanpp01z2:/opt/ibm/HTTPServer2/htdocs";
$servers{"STGLT1"}		= "soanpp01z3:/opt/ibm/HTTPServer/htdocs";
$servers{"STGLT2"}		= "soanps01z3:/opt/ibm/HTTPServer/htdocs";
$servers{"STGAT"}		= "soanpp01z5:/opt/ibm/HTTPServer1/htdocs";
$servers{"EUT3"}		= "soanpp01z1:/opt/ibm/HTTPServer3/htdocs";
$servers{"SAT3"}		= "soanpp01z2:/opt/ibm/HTTPServer3/htdocs";
$servers{"EUT1"}		= "soanpp01z1:/opt/ibm/HTTPServer1/htdocs";
$servers{"SAT1"}		= "soanpp01z2:/opt/ibm/HTTPServer1/htdocs";



$servers{"EUT-CONTENT-T4"} 	= "soanpp01z1:/opt/ibm/WCMHTTPServer/htdocs";
$servers{"REVIEW-T4"} 		= "";
$servers{"PROD1-T4"} 		= "soapdp01z1:/opt/ibm/HTTPServer/htdocs";
$servers{"PROD2-T4"} 		= "soapds01z1:/opt/ibm/HTTPServer/htdocs";
$servers{"EUT-DEV-T4"} 		= "";
$servers{"ST-T4"} 			= "";
$servers{"AT-T4"} 			= "";

$servers{"WEBTEST1"} 		= "sxxxxxx1:/opt/ibm/HTTPServer/htdocs";
$servers{"WEBTEST2"} 		= "sxxxxxx2:/opt/ibm/HTTPServer/htdocs";


our @topLevelDirectories = qw(myrta myRTAApps online-services);





1;
