sub mapWorkareaToUatDirectory {
	my ($wa) = @_;
	for ($wa) {
		if (/www.*digitalXXX/) {
			$docroot = "/applint/project2";
		}
		elsif (m!main.www\.rta.*/WORKAREA/trafficinformation!) {
			$docroot = "/appl/internet";
			$docroot = "/applint/project2";
		}
		elsif (m!main.secure.rms.*/WORKAREA/.*!) {
			$docroot = "/appl/secure";
			#$docroot = "/applint/project3";
		}
               
                elsif (m!main.www\.rta.*/WORKAREA/ccr!) {
                        $docroot = "/appl/internet";
                        #$docroot = "/applint/project1";
                } 
               elsif (m!main.www\.rta.*/WORKAREA/events-test!) {
                        $docroot = "/applint/project1";
                }  
		elsif (m!main.intranet_reskin/WORKAREA/sandbox!) {
			$docroot = "/appl/intranet";
		}
		elsif (m!main.intranet_reskin/WORKAREA/rebranding!) {
			#$docroot = "/applint/project3";
                       $docroot = "/appl/intranet"; 
		}
		elsif (m!main.intranet_reskin/WORKAREA/!) {
			$docroot = "/appl/intranet";
		}
		elsif (/main.intranet\.rta/) {
			$docroot = "DISABLED";
		}
		elsif (/main.frontlinehelp/) {
			$docroot = "/appl/frontlinehelp";
		}
		elsif (m!main.maritime/!) {
			$docroot = "/appl/maritime";
		}

		elsif (m!main.maritime-management!) {
			$docroot = "/applint/project1";
		}
		elsif (m!main.maritime-management.*bau!) {
			$docroot = "/appl/maritimesafety";
		}
		elsif (m!main.firststop!) {
			$docroot = "/applint/project3";
		}
		elsif (m!main.firststop!) {
			$docroot = "/appl/firststop";
		}
		elsif (/main.bicycleinfo/) {
			$docroot = "/appl/bicycleinfo";
		}
		elsif (/main.sydneymotorways/) {
			$docroot = "/appl/motorways";
		}
		elsif (/main.westconnex/) {
			$docroot = "/appl/westconnex";
		}
		elsif (/main.scats/) {
			$docroot = "/appl/scats";
		}
		elsif (/main.myplates/) {
			$docroot = "/appl/myplates";
		}
		elsif (/main.roms/) {
			$docroot = "/appl/roms";
		}
		elsif (/main.freight/) {
			$docroot = "/appl/freight";
		}
		elsif (/main.roadsafety/) {
			$docroot = "/appl/roadsafety";
		}
		elsif (/main.roadprojects/) {
			$docroot = "/appl/internet";
		}
		elsif (/main.techinfo/) {
			$docroot = "/appl/techinfo";
		}
		elsif (/main.webapps/) {
			$docroot = "/appl/webapps";
		}
                elsif (m!main.chiefs-message/WORKAREA/content!) {
                        $docroot = "/appl/intranet/";
			$docroot = "/applint/project2/";
		}
		else {
			$docroot = "DISABLED";
		}
	}
	
	if ($docroot =~ m!/appl/(frontlinehelpXX|intranetXXX)!) {
		$docroot = "DISABLED";
	}
	
	return $docroot;
}



1;
