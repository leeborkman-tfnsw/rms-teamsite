#!/opt/local/sys/iw-home/TeamSite/iw-perl/bin/iwperl
require "/opt/local/sys/iw-home/TeamSite/custom/rta/bin/external_task_call_log.pl";
warn "starting...\n";
#------------------------------------------------------------------------------
#
# 	Name		: 	wfBifCreation.ipl
#
#	Authors		:	Koen Ruymaekers <koen.ruymaekers@didata.com.au>
#
#	Date		: 	Monday, 22 October 2003
#
#	Synopsis	:	Workflow external task script
#				prepopulates the metadata fields only when they are empty!!!
#
#	Input 	:	Files attached to workflow.
#				   Metadata extended attributes for file.
#
# 	Return	:	workflow callback 0
#				   workflow callback 1
#
# 	Subroutine List:
#
# 	Edit History	:
#
# 	Init   No.     Date           Description
# 	====   ===     ==========	   ===========
#
#  	KR     [0]     22/10/2003     initial requirements
#   BC     [1]     ???            clean up description
#   KR     [2]     28/01/2004     populate MT fields with dummy value if nothing in there
#...EW.....[3].....18/03/2004.....clean out backticks from description - crashes UNIX with ex processes when submiited to iwextattr command
#------------------------------------------------------------------------------

use strict ;
use POSIX qw(getpid strftime);
use TeamSite::Config;
use TeamSite::WFtask;
use TeamSite::WFworkflow;
use File::Basename;
use RTA;

require "/usr/local/sys/iw-home/custom/rta/bin/RTA_fast2.pm";
sub getPrimaryDcr_fast;



sub escape {
        my $str = shift;
        $str =~ s!&amp;amp;!&!gis;
        $str =~ s!&amp;!&!gis;
        $str =~ s!&quot;!"!gis;
        $str =~ s!&lt;!<!gis;
        $str =~ s!&gt;!>!gis;
        $str =~ s!&!&amp;!gis;
        $str =~ s!"!&quot;!gis;
        $str =~ s!<!&lt;!gis;
        $str =~ s!>!&gt;!gis;
        return $str;
}



# Debug module
use iwsite::DebugLog;
my $DEBUG = 1;

# Get iwhome and iw_mount
my $iwhome =  TeamSite::Config::iwgethome();
my $iw_mount =  TeamSite::Config::iwgetmount();

# Who am I.
my $scriptName = &basename($0);
$scriptName =~ s/\.[^\.]+$//i;

# Conststants of CLTs used in script
my $IWCONFIG = $iwhome . "/bin/iwconfig";

# State variables in script.
my $error = 0; # Default to no error detected in script.
my $errorComment = 'ERROR: task error detected.'; # Default error comment.
my $taskComment = 'SUCCESS: task completed OK.'; # Default task transition comment.

# Get parameters
my ($jobId, $taskId, $area, $doCallback) = (@ARGV[0..3]);

# Ensure consistent slashes.
$area =~ s/[\\\/]+/\//g if (defined $area);

# Sanity check, ensure $area doesn't already have mount point preceeding it.
$area =~ s/^((?i)\Q$iw_mount\E)//;

# What is my PID
my $pid = getpid();
chomp $pid;

my $debug = new iwsite::DebugLog ($DEBUG, "$iwhome/custom/logs/$scriptName".".log");

# Error check debug object.
if (! defined $debug) {
    exit(1);
    }

$debug->printlog("Begin session $pid $scriptName");

# Do we have reasonable parameters
if ((! defined $jobId) || ($jobId !~ /^\d+$/) ) {
     $debug->printlog("ERROR: Invalid jobId given to $scriptName.");
     exit(1);
   }
if ((! defined $taskId) || ($taskId !~ /^\d+$/) ) {
     $debug->printlog("ERROR: Invalid taskId given to $scriptName.");
     exit(1);
   }
if ((! defined $area) || ($area !~ /\/default\d?\/main\/.+$/) ) {
     $debug->printlog("ERROR: Invalid area given to $scriptName.");
     exit(1);
   }
$debug->printlog("jobid: $jobId");
$debug->printlog("taskid: $taskId");
$debug->printlog("area: $area");

# Create wf and task objects
my($wf) = TeamSite::WFworkflow->new($jobId);
if (! defined $wf) {
    $debug->printlog("ERROR: creating workflow job object.");
    exit(1);
   }

my($task) = TeamSite::WFtask->new($taskId);
if (! defined $task) {
    $debug->printlog("ERROR: creating workflow task object.");
    exit(1);
   }

$debug->printlog("BEGIN $pid: script body.");

############################################################################
# Start script body
############################################################################

#this is an array with the extended attributes to be generated
my @MTextattr = ("TeamSite/Metadata/aglssubject",
                 "TeamSite/Metadata/aglsdescription",
                 "TeamSite/Metadata/Title");

# Get task files
my (@wfFiles) = $task->GetFiles();
my (@fileRoots); # this array will contain all the fileroots (one per directory under /html/)
my $dcr;
foreach my $file (@wfFiles) {
    my $comment;

# Tidy up paths
    chomp $file;

    $file =~ s/^\s*//;
    $file =~ s/\s*$//;

    $file =~ s/^[\\\/]+//;

    my $srcFile = "$iw_mount$area/$file";

	# skip deletions
	next unless (-e $srcFile);
	my $cmd;
	my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks);

    my $vpathFile = $srcFile;
    $vpathFile =~ s!$iw_mount!!;
	if ($vpathFile =~ m!templatedata/!) {
		$debug->printlog("vpathFile :: $vpathFile");
		$debug->printlog ("File is a DCR. Skipping $vpathFile\n");
		$dcr = "";
		next;
	} else {
		$dcr = RTA_fast::getPrimaryDcr_fast("$vpathFile");
		my $sourceFile = ($dcr?$dcr:$vpathFile);
		$debug->printlog ("Sourcefile for metadata is $sourceFile\n");
		if ($dcr) {
			$debug->printlog ("File's DCR is $dcr\n");



			my $idol_title = "";
			$idol_title = escape(RTA_fast::getValue($dcr, "Long Title"));
			$idol_title = escape(RTA_fast::getValue($dcr, "Short Title")) unless $idol_title;
			$debug->printlog ("Long Title is $idol_title\n");
			warn("title\n");
			$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_title="$idol_title" "$vpathFile"~;
            $debug->printlog ("CMD:$cmd");
            $debug->printlog (`$cmd`);

			my $idol_description = "";
			$idol_description = escape(RTA_fast::getValue($dcr, "Page Description"));
			$debug->printlog ("Long Title is $idol_description\n");
			warn("description\n");
			$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_description="$idol_description" "$vpathFile"~;
            $debug->printlog ("CMD:$cmd");
            $debug->printlog (`$cmd`);
			warn("subject\n");
			my $subject = escape(RTA_fast::getValue($dcr, "Page Subject"));
			$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_subject="$subject" "$vpathFile"~;
            $debug->printlog ("CMD:$cmd");
            $debug->printlog (`$cmd`);


		}


		($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks)= stat($sourceFile);
		my $idol_createddate = `/opt/local/sys/iw-home/TeamSite/bin/iwextattr -g  TeamSite/Metadata/idol_createddate "$vpathFile"`;
		$debug->printlog ("Create date is currently: $idol_createddate\n");
		if ($idol_createddate) {
			$debug->printlog ("Create date is already set. Leave it alone.\n");
		} else {
			warn("creation\n");
			my ($S, $M, $H, $d, $m, $Y) = localtime($mtime);
			$m += 1;
			$Y += 1900;
			my $idol_createddate = sprintf("%04d-%02d-%02d", $Y,$m, $d);
			warn("$idol_createddate\n");
			$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_createddate="$idol_createddate" "$vpathFile"~;
			$debug->printlog ("CMD:$cmd");
			$debug->printlog (`$cmd`);
		}
		warn("updated\n");
		my ($S, $M, $H, $d, $m, $Y) = localtime($mtime);
		$m += 1;
		$Y += 1900;
		my $idol_lastupdateddate = sprintf("%04d-%02d-%02d", $Y,$m, $d);
		warn("$idol_lastupdateddate\n");
		$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_lastupdateddate="$idol_lastupdateddate" "$vpathFile"~;
		$debug->printlog ("CMD:$cmd");
		$debug->printlog (`$cmd`);

		warn("extension\n");
		my $extension = $vpathFile;
		$extension =~ s/^.*\.//gis;
		$extension = uc $extension;
		$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_format="$extension" "$vpathFile"~;
		$debug->printlog ("CMD:$cmd");
		$debug->printlog (`$cmd`);

		warn("URL\n");
		my $url = $vpathFile;
		$url =~ s!^.*/WORKAREA/[^/]+/html/!!gis;
		#$url = "http://home.rta.nsw.gov.au/$url";
		$url = "/$url";
		$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_url="$url" "$vpathFile"~;
		$debug->printlog ("CMD:$cmd");
		$debug->printlog (`$cmd`);


		warn("source\n");
		$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_source="Intranet" "$vpathFile"~;
		$debug->printlog ("CMD:$cmd");
		$debug->printlog (`$cmd`);

		next unless ($file =~ /\.html?$/);
		warn("summary\n");
		my $summary = escape(RTA_fast::getValue($dcr, "Summary"));
		if ($summary !~ /[A-Z]/) {
			$summary = `/usr/bin/cat $vpathFile`;
			$summary =~ s!</title>.*<body!</title><body!is;
			$summary =~ s!</title>.*<.-- #BeginMain -->!</title><\!-- #BeginMain -->!is;
			$summary =~ s!</title>.*<.-- #BeginContent -->!</title><\!-- #BeginContent -->!is;
			$summary =~ s!<[^>]*>! !gis;
			$summary =~ s!^\s*((\S+\s+){1,60}).*$!$1!is;

		}
    $summary =~ s!\&gt;!>!gis;
    $summary =~ s!\&lt;!<!gis;
    $summary =~ s!\&amp;!\&!gis;
    $summary =~ s!\&quot;!'!gis;
	$summary =~ s/[^[:ascii:]]\x80[\x99\x98]/'/gis;
	$summary =~ s/[^[:ascii:]]+/-/gis;

    $summary =~ s!<\!--.*?-->!!gis;
    $summary =~ s!^.*?(<h\d)!$1!is;
    $summary =~ s!<button class="visible-xs.*?button>!!is;
    $summary =~ s!<foot.*!!is;
    $summary =~ s!<\!-- #EndContent -->.*!!is;

    $summary =~ s!<[^>]*?>! !gis;
    $summary =~ s!<script.*?</script>!!gis;
    $summary =~ s!<style.*?</style>!!gis;
    $summary =~ s!\s+! !gis;
    #warn ("SUMMARY:$summary\n");
    #warn ("got $1\n");
    #warn ("SUMMARY SHORTENED:$summary\n");
		$summary =~ s!"!'!gis;
		$summary =~ s!\s+! !gis;
		$cmd = qq~/opt/local/sys/iw-home/TeamSite/bin/iwextattr -s  TeamSite/Metadata/idol_summary="$summary" "$vpathFile"~;
		$debug->printlog ("CMD:$cmd");
		$debug->printlog (`$cmd`);




	}

    if (-e $srcFile) {     #make sure this is not a deleted file
        my $cmd = "$iwhome/bin/iwextattr -l " . $srcFile;
        my $extattr = `$cmd`;
        foreach my $thisextattr (@MTextattr) {
            if ($extattr =~ /$thisextattr/) {
                $debug->printlog("extended attribute exists: $thisextattr");
            }
            else {
                # run metatagger for this attribute
                my ($shortextattr) = $thisextattr =~ /TeamSite\/Metadata\/(.*)/;
                my $cmd = "$iwhome/metatagger/bin/iwmtbatch -save -suggest_by_tag $shortextattr $srcFile";
                $debug->printlog ("running: $cmd\n");
                `$cmd`;
            }
        }
        ###########################
        # BC - [1] ---- 21/11/2003
        ###########################
        my $cmd = "$iwhome/bin/iwextattr -g \"TeamSite/Metadata/aglsdescription\" " . $srcFile;
        my $newaglsdescription = `$cmd`;
        if ($newaglsdescription =~ /[a-zA-Z0-9]/) {
            $newaglsdescription =~ s!Attribute\s+TeamSite/Metadata/aglsdescription not found!!;

            $newaglsdescription =~ s/\.\s,\s/\. /g;
            $newaglsdescription =~ s/\.,/\. /g;
            $newaglsdescription =~ s/\.,\s/\. /g;

            # try this but I don't think it works
            $newaglsdescription =~ s/[^\x20-\x7e]+//gi;

            #so......now get rid of everything else
            $newaglsdescription =~ s!�!!gis;
			$newaglsdescription =~ s!� !!gis;
			$newaglsdescription =~ s!Â!!gis;
			$newaglsdescription =~ s! !!gis;
            $newaglsdescription =~ s!�!!gis;
            $newaglsdescription =~ s!‘!'!gis;
            $newaglsdescription =~ s!’!'!gis;
            $newaglsdescription =~ s!“!"!gis;
            $newaglsdescription =~ s!”!"!gis;
            $newaglsdescription =~ s!…!...!gis;
            $newaglsdescription =~ s!–!-!gis;
            $newaglsdescription =~ s!â!�!gis;
            $newaglsdescription =~ s!\s+! !g;
            $newaglsdescription =~ s!\`.! !g;  # EW Here we are stripping out the backtick character because it crashes iwextattr
            $cmd = "$iwhome/bin/iwextattr -s \"TeamSite/Metadata/aglsdescription=$newaglsdescription\" " . $srcFile;

            # if the description still has a printable set of chars....
            if ($newaglsdescription =~ /[a-zA-Z0-9]/) {
                my $result = `$cmd`;
            }
         }
#        ###########################
#        # KR - [2] ---- 28/01/2004
#        #  fill empty fields with dummy values
#        ###########################
#        my $cmd = "$iwhome/bin/iwextattr -g \"TeamSite/Metadata/aglsdescription\" " . $srcFile;
#        my $maybeemptyaglsdescription = `$cmd`;
#        if ($maybeemptyaglsdescription eq "") {
#            $cmd = "$iwhome/bin/iwextattr -s \"TeamSite/Metadata/aglsdescription=NoDescription\" " . $srcFile;
#            my $result = `$cmd`;
#            $debug->printlog("NoDescription for $srcFile\n$result");
#        }
#
#        $cmd = "$iwhome/bin/iwextattr -g \"TeamSite/Metadata/Title\" " . $srcFile;
#        my $maybeemptyTitle = `$cmd`;
#        if ($maybeemptyTitle eq "") {
#            $cmd = "$iwhome/bin/iwextattr -s \"TeamSite/Metadata/Title=NoTitle\" " . $srcFile;
#            my $result = `$cmd`;
#            $debug->printlog("NoTitle for $srcFile\n$result");
#        }
    }
    else {
         $debug->printlog("deleted file: $srcFile");
    }
}

# All done

############################################################################
# End script body
############################################################################

if (! $error) {
    # success
    $debug->printlog("END $pid: Callback 0, Comment: $taskComment");
	if ($doCallback) {
		$task->CallBack(0, "$taskComment");
	}
    exit(0);
    }
else{
  $debug->printlog("END $pid: Callback 1, Comment: $errorComment");
  $task->CallBack(1, "$errorComment");
  exit(1);
}
