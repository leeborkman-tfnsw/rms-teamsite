#!/usr/local/sys/iw-home/iw-perl/bin/iwperl
#------------------------------------------------------------------------------
# Copyright 1999-2002 Interwoven Inc.
# All rights reserved.
#
# 	Name		: 	wfMetadataFields_intranet.ipl
# 
#	Author		:	Koen Ruymaekers <koen.ruymaekers@didata.com.au>
#
#	Date		: 	Wednesday, 8 August 2003	
# 
#	Synopsis	:	Workflow external task script
#				   that extracts and sets Metadata extended attributes for intranet files
#
#	Input 		:	Normal workflow arguments.
#				   Metadata extended attributes for file.
#					
# 	Return		:	workflow callback 0
#				workflow callback 1
#
# 	Subroutine List: 
#
# 	Edit History	:
#
# 	Init   No.     Date           Description
# 	====   ===     ==========	   ===========
#
#  KR     [0]     08/10/2003     initial requirements
#
#------------------------------------------------------------------------------

use strict ;
use POSIX qw(getpid strftime);
use TeamSite::Config;
use TeamSite::WFtask;
use TeamSite::WFworkflow;
use File::Basename;
use iwsite::iwea;
use iwsite::iwldap;
use Date::Manip; 
use RTA; 

# set the time zone for Date::Manip
&Date_Init("TZ=GMT" , "DateFormat=nonUS");

# Debug module
use iwsite::DebugLog;
my $DEBUG = 1;

# Get iwhome and iw_mount 
my $iwhome =  TeamSite::Config::iwgethome();
my $iw_mount =  TeamSite::Config::iwgetmount();

# Who am I.
my $scriptName = &basename($0);
$scriptName =~ s/\.[^\.]+$//i;

# Conststants of CLTs used in script
my $IWCONFIG = $iwhome . "/bin/iwconfig";
my $IWBIN = $iwhome . "/bin";

# State variables in script.
my $error = 0; # Default to no error detected in script.
my $errorComment = 'ERROR: task error detected.'; # Default error comment.
my $taskComment = 'SUCCESS: task completed OK.'; # Default task transition comment.

#global variables
my $creator;

# Get parameters
my ($jobId, $taskId, $area) = (@ARGV[0..2]);

# Ensure consistent slashes.
$area =~ s/[\\\/]+/\//g if (defined $area);

# Sanity check, ensure $area doesn't already have mount point preceeding it.
$area =~ s/^((?i)\Q$iw_mount\E)//;

# What is my PID
my $pid = getpid();
chomp $pid;

my $debug = new iwsite::DebugLog ($DEBUG, "$iwhome/custom/logs/$scriptName".".log");

# Error check debug object.
if (! defined $debug) {
    exit(1);
    }

$debug->printlog("Begin session $pid $scriptName");

# Do we have reasonable parameters
if ((! defined $jobId) || ($jobId !~ /^\d+$/) ) {
     $debug->printlog("ERROR: Invalid jobId given to $scriptName.");
     exit(1);
   }
if ((! defined $taskId) || ($taskId !~ /^\d+$/) ) {
     $debug->printlog("ERROR: Invalid taskId given to $scriptName.");
     exit(1);
   }
if ((! defined $area) || ($area !~ /\/default\/main\/.+$/) ) {
     $debug->printlog("ERROR: Invalid area given to $scriptName.");
     exit(1);
   }
 
$debug->printlog("jobid: $jobId");
$debug->printlog("taskid: $taskId");
$debug->printlog("area: $area");

# Create wf and task objects
my($wf) = TeamSite::WFworkflow->new($jobId);
if (! defined $wf) {
    $debug->printlog("ERROR: creating workflow job object.");
    exit(1);
   }

my($task) = TeamSite::WFtask->new($taskId);
if (! defined $task) {
    $debug->printlog("ERROR: creating workflow task object.");
    exit(1);
   }

$debug->printlog("BEGIN $pid: script body.");

############################################################################
# Start script body
############################################################################

# Get templatedata root
my $templatedata = &getTemplatedataRoot();

# Get the Contributor
my $contributor = $task->GetVariable("Contributor");
$debug->printlog("contributor :: $contributor");

# Get task files
my (@wfFiles) = $task->GetFiles();

# For each file check file against filtering rules, and insert <meta> tags
# into html if rules are passed OK.
foreach my $file (@wfFiles) {
    
 # Variable for comments
 my $comment;
    
  # Tidy up paths
  chomp $file; 
  $file =~ s/^\s*//; 
  $file =~ s/\s*$//;
  $file =~ s/^[\\\/]+//;
    
  my $srcFile = "$iw_mount$area/$file";
  my $vpathFile = $srcFile;
  $vpathFile =~ s!$iw_mount!!;
  $debug->printlog("vpathFile :: $vpathFile");

  # Check writable
  if (! -w $srcFile) {
    $comment .= "Cannot write $srcFile";
    $taskComment .= "\n$comment";
    $debug->printlog("ERROR $pid: $comment");
    next;
  }
  
  if (! -f $srcFile) {
    $comment .= "Not a regular file $srcFile";
    $taskComment .= "\n$comment";
    $debug->printlog("ERROR $pid: $comment");
    next;
  }
 
  # Check if file this is a DCR, then skip
  if ($srcFile =~ /\Q$area\E\/\Q$templatedata\E\/[^\/]+\/[^\/]+\/data\//) {
    $comment .= "File is DCR $srcFile";
    $taskComment .= "\n$comment";
    $debug->printlog("SKIPPING $pid: $comment");
    next;
  }

  # Read file extended attributes
   my ($exist_eaHash, $resultCode) = &iwsite::iwea::eaRead($vpathFile);
   
  # new hash for updated values 	
  my %new_eaHash = ();

  # ===============================================================================
  # CREATOR
  # ===============================================================================
  # Determine if the Creator has been defined
  # get the creator
  $creator = &getFileCreator($vpathFile);
  chomp ($creator);
  my $creatorrecord = RTA::searchCorpLdap ('cn', '', $creator, '', 'fullname');
  my $creatorfullname = "";
  for my $dn (sort keys %$creatorrecord) {
    $creatorfullname = $creatorrecord->{$dn}->{'fullname'};
  }

  if (!$creatorfullname) { chomp ($creator); $creatorfullname = ">$creator<"};

  chomp $creatorfullname;
  $debug->printlog("creator ===> $creatorfullname");
  $new_eaHash{'TeamSite/Metadata/aglscreator'} = $creatorfullname;
	
  # ===============================================================================
  # PUBLISHER
  # ===============================================================================
  # The Publisher is defined as the directorate (rtadirectorate) for the contributor in the corporate LDAP directory 

  my $publisherrecord = RTA::searchCorpLdap ('cn', '', $contributor, '', 'rtadirectorate', 'rtabranch');
  my $publisher = "";
  for my $dn (sort keys %$publisherrecord) {
    $publisher = $publisherrecord->{$dn}->{'rtabranch'};
  }

  if (!$publisher) {
    for  my $dn (sort keys %$publisherrecord) {
      $publisher = $publisherrecord->{$dn}->{'rtadirectorate'};
    }
  }

  if (!$publisher) {$publisher = ">No directorate or branch defined for $contributor<"};


  $debug->printlog("publisher ===> $publisher");
  $new_eaHash{'TeamSite/Metadata/aglspublisher'} = $publisher;

  # ===============================================================================
  # CONTRIBUTOR
  # ===============================================================================
  if (defined $contributor) {
    chomp $contributor;

    my $contributorrecord = RTA::searchCorpLdap ('cn', '', $contributor, '', 'fullname');
    my $contributorfullname = "";
    for my $dn (sort keys %$contributorrecord) {
      $contributorfullname = $contributorrecord->{$dn}->{'fullname'};
    }
    if (!$contributorfullname) { chomp ($contributor); $contributorfullname = ">$contributor<"};

    $debug->printlog("contributor ===> $contributorfullname");
    $new_eaHash{'TeamSite/Metadata/aglscontributor'} = $contributorfullname;
  } 
  else {$errorComment = " Contributor not defined"; $error=1;}

  # ===============================================================================
  # IDENTIFIER
  # ===============================================================================
  # Identifier is the full path from web root. ie the WORKAREA
  # This may need to have the webserver deployment root appended
  # the docroot needs to be stripped so we have a real path

  # don't set the docroot if extended attribute "autoidentifier" is "No"
  my $refreshidentifier = 1;
  if ( defined ${$exist_eaHash}{'TeamSite/Metadata/autoidentifier'}) {
    if (${$exist_eaHash}{'TeamSite/Metadata/autoidentifier'} eq "No") {
      $refreshidentifier = 0; 
      $debug->printlog("Not refreshing identifier because autoidentifier = No");
    }
  }

  if ($refreshidentifier) {
    my $identifier = $file;
    $identifier =~ s!\\!\/!g;
    my $strippedIdentifier = stripDocroot($identifier);
    $debug->printlog("identifier ===> $strippedIdentifier");
    $new_eaHash{'TeamSite/Metadata/aglsidentifier'} = $strippedIdentifier ;
  }
   
  # ===============================================================================
  # VFILENAME
  # ===============================================================================	
  # 
  my $vfilename = $file;
  $vfilename =~ s!\\!\/!g;
  my $strippedVfilename = stripDocroot($vfilename);
  my ($shortVfilename) = $strippedVfilename =~ /^.*\/(.*)$/;
  $debug->printlog("vfilename ===> $shortVfilename");
  $new_eaHash{'TeamSite/Metadata/vfilename'} = $shortVfilename ;
   
  # ===============================================================================
  # DATE.CREATED
  # ===============================================================================	
  # Determine if the Date Created has been defined
  if (! defined ${$exist_eaHash}{'TeamSite/Metadata/aglsdatecreated'}) {
    my $createDate = getFileCrDate($vpathFile);
    if (defined $createDate) {
      my $formatedCrDate = &UnixDate($createDate, "%Y-%m-%d"); 
      $debug->printlog("createDate ===> $formatedCrDate");
      $new_eaHash{'TeamSite/Metadata/aglsdatecreated'} = $formatedCrDate;
    } 
    else {$errorComment = " createDate not defined for $vpathFile"; $error=1;}
  }

  # ===============================================================================
  # RESOURCE MANIFIESTATION.FORMAT
  # ===============================================================================
  my ($fileextension) = $file =~ /\.(...)$/;
  open (MIMEFILE, "/usr/local/sys/iw-home/custom/rta/bin/mime.types") or die ("cannot open mime file");
  my $mimeapp = "";
  while (<MIMEFILE>) {
    chomp;
    next if /^#/;
    next if /^$/;
    my ($app, $exts) = split /exts=/;
    my @extensions = split (/,/, $exts);
    foreach my $thisextension (@extensions) {
      if ($fileextension eq $thisextension) {
        $mimeapp = $app;
      }
    }
  }
  if ($mimeapp) {
    my ($aglsformat) = $mimeapp =~ /type=(.*)/;
    $new_eaHash{'TeamSite/Metadata/aglsformat'} = $aglsformat;
  }
  
  # ===============================================================================
  # FUNCTION
  # ===============================================================================
  my $keywordsUIDstring = ${$exist_eaHash}{'TeamSite/Metadata/aglssubject_codes'};
  if ($keywordsUIDstring) {
    print ">$keywordsUIDstring<\n"; 
    #$new_eaHash{'TeamSite/Metadata/aglsfunction'} = $keywordsUID;
    my @keywordsUIDs = split (/,/,$keywordsUIDstring);
    foreach my $keywordUID (sort @keywordsUIDs) {
      chomp($keywordUID);
      $keywordUID =~ s/^ +//;
      print "$keywordUID\n";
    }
  }





  # ===============================================================================
  # DATE MODIFIED
  # ===============================================================================
  my $datemodified = &getFileModifiedDate($vpathFile);
  if (defined $datemodified) {
    chomp $datemodified;
    $debug->printlog("datemodified ===> $datemodified");
    $new_eaHash{'TeamSite/Metadata/aglsdatemodified'} = $datemodified;
  }
  else {$errorComment = " Creator not defined"; $error=1;}

  # ===============================================================================
  # DATE ISSUED
  # ===============================================================================
	
  my $dateIssued = &ParseDate("today");
  $dateIssued = &UnixDate($dateIssued, "%Y-%m-%d"); 
  $debug->printlog("Issued Date ===> $dateIssued");

  $new_eaHash{'TeamSite/Metadata/aglsdateissued'} = $dateIssued;

  # ===============================================================================
  # that's it
  # ===============================================================================
   
  # set the EA's
  my ($successCount, $newresultCode) = &iwsite::iwea::eaWrite($vpathFile ,\%new_eaHash); 

  # test the result of the EA update 
  if ($newresultCode == 1) {$comment = "EA update return code = $newresultCode \n$successCount metadata elements updated on $file";}
  elsif ($newresultCode == - 1) { $errorComment = " $newresultCode :: $file not readable"; $error=1;}
  elsif ($newresultCode == - 2) { $errorComment = " $newresultCode :: iwextattr CLT not executable"; $error=1;}
  elsif ($newresultCode == - 3) { $errorComment = " $newresultCode :: iwextattr CLT returned error for $file"; $error=1;}

  my $taskComment .= "\n$comment";
 
  $debug->printlog("Set Automatic Metadata $pid: $file.\n$comment");
}
# All done.

############################################################################
# End script body
############################################################################

if (! $error) {
    # success
  $debug->printlog("END $pid: Callback 0, Comment: $taskComment");
  $task->CallBack(0, "$taskComment");
  exit(0);
  }
else {
  $debug->printlog("END $pid: Callback 1, Comment: $errorComment");
  $task->CallBack(1, "$errorComment");
  exit(1);
}

sub getTemplatedataRoot {
    my $cmd = "$IWCONFIG teamsite_templating data_root";
    my $cmdResult = `$cmd 2>&1`; chomp $cmdResult; 
    my $templatedataRoot = 'templatedata';
    if ((defined $cmdResult) && ($cmdResult !~ /^\s*$/) &&
    	($cmdResult !~ /variable not found/i)) {
        $cmdResult =~ s/^\s*["']//; $cmdResult =~ s/\s*["']$//;
        $templatedataRoot = $cmdResult;
        }
    return $templatedataRoot;
    }

sub getFileCreator {
    my ($file) = (shift);
    return (undef) if (!defined $file);
    my $cmd = "$iwhome/bin/iwattrib "; $cmd =~ s![\\\/]+!\/!g;
    my $creatorstr = "$cmd \"$file\" creator";
	    $creatorstr =~ s![\\\/]+!\/!g;
	 my $creator = `$creatorstr`;
	 if ((defined $creator) && ($creator !~ /^\s*$/)){
        return $creator;
    }
}

sub getFileModifiedDate {
    my ($file) = (shift);
    return (undef) if (!defined $file);
    my %months = ( Jan => "01",
                   Feb => "02",
                   Mar => "03",
                   Apr => "04",
                   May => "05",
                   Jun => "06",
                   Jul => "07",
                   Aug => "08",
                   Sep => "09",
                   Oct => 10,
                   Nov => 11,
                   Dec => 12);

    my $cmd = "$iwhome/bin/iwattrib "; $cmd =~ s![\\\/]+!\/!g;
    my $modifieddatecmd = "$cmd \"$file\" moddate";
    $modifieddatecmd =~ s![\\\/]+!\/!g;
    my $modifieddate = `$modifieddatecmd`;
    if ((defined $modifieddate) && ($modifieddate !~ /^\s*$/)){
      my ($weekday, $month, $date, $time, $year) = split(/\s+/,$modifieddate);
      if (length($date) == 1) {$date = "0" . $date;}
      $month = $months{$month};
      return "$year-$month-$date";
    }

}

sub getFileCrDate {
    my ($file) = (shift);
    return (undef) if (!defined $file);
    my $cmd = "$iwhome/bin/iwattrib "; $cmd =~ s![\\\/]+!\/!g;
    my $crdatestr = "$cmd \"$file\" crdate";
     	 $crdatestr =~ s![\\\/]+!\/!g;
	 my $crdate = `$crdatestr`;
	 if ((defined $crdate) && ($crdate !~ /^\s*$/)){
        return $crdate;
    }
}

sub stripDocroot {
	my ($url) = @_;
	$url =~ s/^[\/\\]?html//gs;
	return $url;
}
