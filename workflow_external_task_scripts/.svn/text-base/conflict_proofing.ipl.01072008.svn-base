#!/usr/local/sys/iw-home/iw-perl/bin/iwperl
#------------------------------------------------------------------------------
# Copyright 1999-2000 Interwoven Inc.
# All rights reserved.
#
# 	Name		: 	wfAttachDcr.ipl
# 
#	Authors		:	Peter Debus <pdebus@interwoven.com>
#
#	Date		: 	13/03/2002	
# 
#	Synopsis	:	Workflow external task script
#				that attaches DCRs associated with 
#				other files (html, jsp, inc etc)
#				to the workflow,
#	Input 	:		NA
#					
# 	Return	:		workflow callback 0
#
# 	Subroutine List: 
#
# 	Edit History	:
#
# 	Init 	No.  	Date 			Description
# 	====  	===  	==========		===========
#
#  	PD	[0]  	13/03/2002		initial requirements
#       KR      [1]     16/10/2002              changed the permissions to 664 for template-generated files
#                                               this is required because there is a bug in TeamSIte that sets 
#                                               default permission for the html file to 644, making it impossible for
#                                               someone down the workflow to make changes
#
#------------------------------------------------------------------------------

use strict ;
use POSIX qw(getpid strftime);
use TeamSite::Config;
use TeamSite::WFtask;
use TeamSite::WFworkflow;
use File::Basename;
use iwsite::iwea ;

# Debug module
use iwsite::DebugLog;
my $DEBUG = 1;

# Get iw_home iw_mount and app_home
my $iw_home =  TeamSite::Config::iwgethome();
my $iw_mount =  TeamSite::Config::iwgetmount();
$iw_home =~ s/[\\\/]+/\//g;
$iw_mount =~ s/[\\\/]+/\//g;

# Who am I.
my $scriptName = &basename($0);
$scriptName =~ s/\.[^\.]+$//i;

# Conststants of CLTs used in script
my $IWCONFIG = $iw_home . "/bin/iwconfig";

# State variables in script.
my $error = 0; # Default to no error detected in script.
my $errorComment = 'ERROR: task error detected.'; # Default error comment.
my $taskComment = 'SUCCESS: task completed OK.'; # Default task transition comment.

# Get parameters

my ($jobId, $taskId, $area) = (@ARGV[0..2]);

my $debug = new iwsite::DebugLog ($DEBUG, "$iw_home/custom/logs/$scriptName".".log");

# Ensure consistent slashes.
$area =~ s/[\\\/]+/\//g if (defined $area);

# Sanity check, ensure $area doesn't already have mount point preceeding it.
$area =~ s/^((?i)\Q$iw_mount\E)//;

# What is my PID
my $pid = getpid();
chomp $pid;


# Error check debug object.
if (! defined $debug) {
    exit(1);
    }

$debug->printlog("Begin session $pid $scriptName");
$debug->printlog("jobId: $jobId\n");
$debug->printlog("taskId: $taskId\n");
$debug->printlog("area: $area\n");

# Do we have reasonable parameters
if ((! defined $jobId) || ($jobId !~ /^\d+$/) ) {
    $debug->printlog("ERROR: Invalid jobId given to $scriptName.");
    exit(0);
    }
if ((! defined $taskId) || ($taskId !~ /^\d+$/) ) {
    $debug->printlog("ERROR: Invalid taskId given to $scriptName.");
    exit(0);
    }
if ((! defined $area) || ($area !~ /\/default\/main\/.+$/) ) {
    $debug->printlog("ERROR: Invalid area given to $scriptName.");
    exit(0);
    }

# Create wf and task objects
my($wf) = TeamSite::WFworkflow->new($jobId);
if (! defined $wf) {
    $debug->printlog("ERROR: creating workflow job object.");
    exit(1);
    }

my($task) = TeamSite::WFtask->new($taskId);
if (! defined $task) {
    $debug->printlog("ERROR: creating workflow task object.");
    exit(1);
    }

$debug->printlog("BEGIN $pid: script body.");


############################################################################
# Start script body
############################################################################


# Get task files
my (@wfFiles) = $task->GetFiles();

# For each file retrieve DCR associated with file. May be NA.
foreach my $file (@wfFiles) {
    # Variable for comments
    my $comment = "";
    # Tidy up paths
    chomp $file; $file =~ s/^\s+//; $file =~ s/\s+$//;
    $file =~ s/[\\\/]+/\//g; $file =~ s/^\///; 
    my $srcFile = "$iw_mount$area/$file";
    my $vPath = "$area/$file";
    `/usr/local/sys/iw-home/bin/iwunlock $vPath`;
}
# All done.

############################################################################
# End script body
############################################################################
if (! $error) {
    $task->CallBack(0, "$taskComment"); 
    $debug->printlog("END $pid: Callback 0, Comment: $taskComment");
    exit(0);
    }

$task->CallBack(1, "$errorComment");
$debug->printlog("END $pid: Callback 1, Comment: $errorComment");
exit(1);

