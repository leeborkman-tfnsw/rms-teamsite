<template_script><![CDATA[
#------------------------------------------------------------------------------
#  File      external_task_test.wft 
#  Use       test harness for an external task
#
#  This template uses the following HTTP request keys:
#
#    iw_user           The name (user id) of the current user
#    iw_workarea       The workarea's vpath
#    iw_file           A list of files, each relative to iw_workarea
#    iw_template_file  The path to this template file relative to
#                       iw-home/local/config/wft
#
#  Copyright 2003 Interwoven Inc.
#  All rights reserved.
#------------------------------------------------------------------------------

use TeamSite::Config;

# Set $debugging to 1 to enable debugging; 0 to disable.
my $debugging = 1;

my $temp_dir = (($^O eq "MSWin32") ? "C:/temp" : "/tmp");
my $wft_base_name = __VALUE__('iw_template_file');
# Split the path into the part before the last slash (the directory) and
# the part after (the filename).
$wft_base_name =~ m|^(.*)[\\/]([^\\/]+)$|;
my $this_dir = $1;
$wft_base_name = $2;
$wft_base_name =~ s|\.[^\.]+$||; # Remove the extension

my $default_debug_file = "$temp_dir/$wft_base_name.xml";

my $iwhome = TeamSite::Config::iwgethome();
# The path to TeamSite Perl.
my $iwperl = "$iwhome/iw-perl/bin/iwperl";
my $wft_dir = TeamSite::Config::iwgetlocation("iwconfigs")."/wft";

### ATTENTION: This is the path to the sample external program, and a list
###            of initial arguments that are passed to the program.
###            You will want to change these if you use a different program.
my @arguments = ("red", "white", "and blue");
my $test_command = "$iwperl $wft_dir/$this_dir/sample_external_task.ipl";
for my $arg ( @arguments ) {
    # Put quotes around each argument in case there are spaces.
    $test_command .= ' "' . $arg . '"';
}

CGI_info(
    tag_table_options   => "border=0", 
    error_label_bgcolor => "#EEEEEE" ,
    valid_bgcolor       => "#EEEEEE" ,
);

# Add debugging fields to the form if debugging has been switched on.
if ($debugging) { 
    TAG_info(
        iw_output_file =>
             [ label       => '',
               html        => 'Write job spec to:'
                            . '<input type="text" value="'
                            . $default_debug_file . '" size="40">',
               is_required => 'false',
             ],
        iw_debug_mode  =>
             [ label       => '',
               html        => '<input type="checkbox" value="true">'
                            . ' Display job spec instead',
               is_required => 'false',
             ],
    );
}

TAG_info(
    # The string to be used as the job's description.
    iw_desc =>  
         [ label       => '',
           html        => 'Job Description:<br>'
                          . '<textarea rows=3 cols=50></textarea>',
           is_required => 'true',
           error_msg   => 'Please enter a description',
         ],
    # The maximum number of iterations.
    iterations =>  
         [ label       => '',
           html        => 'Maximum number of iterations: '
                          . '<input type="text" value="3" size="2"><br>&nbsp;',
           # Input must be one or two digits, optionally with whitespace
           # before and after.
           valid_input => '/^\w*\d{1,2}\w*$/',
           is_required => 'true',
           error_msg   => 'Please enter an integer from 1 to 99',
         ],
);

# Make sure that this form gets displayed at least once, even if all
# required values are already available.
if (!defined(__VALUE__(iw_first_time))) {
    TAG_info(
        first_time_hidden_placeholder  =>
             [ html  =>  "<input type=hidden value=''>",
               is_required => 'true',
             ],
    );
}

# Take the list of files in 'iw_file' and sort them in unencoded form.
my @submit_file = (__ELEM__('iw_file') ? sort __VALUE__('iw_file') : ());

# For each file, add a textarea in the job creation form for file comments.
# A heading ("Individual File Comments") preceeds the first file.
for (my $i=0; $i<@submit_file; ++$i) {
    TAG_info(
        "File_comment_$i" => 
            [ html        => 'Comment for '
                             . TeamSite::CGI_lite::escape_html_data($submit_file[$i])
                             . '<br><textarea rows=3 cols=50></textarea>',
              error_msg   => 'Please enter a comment.',
              is_required => 'false',
              label       => '',
            ],
    );
}

]]></template_script><?xml version="1.0" standalone="no"?>
<!DOCTYPE workflow SYSTEM "iwwf.dtd">

<workflow name="External Task Test"
          owner="__TAG__('iw_user');"
          creator="__TAG__('iw_user');"
          description="__TAG__('iw_desc');">
    <!-- Some example job variables -->
    <variables>
        <variable key="Department" value="Marketing"/>
        <!-- "M&M's" is a trademark of Mars Inc. -->
        <variable key="Product" value="M&amp;M's"/>
    </variables>

    <externaltask name="External Task" 
              owner="__TAG__('iw_user');" 
              description="Let a user run the external program"
              start="t"
              readonly="t">
        <areavpath v="__TAG__('iw_workarea');"/>
        <successors>
            <successorset description="Finish Job">
                <succ v="End"/>
            </successorset>
            <successorset description="Retry Later">
                <succ v="Wait for next test"/>
            </successorset>
        </successors>
        <command v='__INSERT__($test_command);'/>
        <template_script><![CDATA[
            if (@submit_file != 0)
            {
                __INSERT__("<files>\n");
                for (my $i=0; $i < @submit_file; ++$i)
                {
                    __INSERT__(" " x 12 . "<file path='$submit_file[$i]' "
                               . "comment='__TAG__(File_comment_$i);'/>\n");
                }
                __INSERT__(" " x 8 . "</files>");
            }
        ]]></template_script>
        <!-- Some example task variables -->
        <variables>
            <variable key="sign" value="Virgo"/>
            <variable key="city" value="Paris"/>
            <variable key="MAX_ITERATIONS" value="__TAG__('iterations');"/>
        </variables>
    </externaltask>

    <usertask name="Wait for next test"
              owner="__TAG__('iw_user');"
              description="Wait until ready to retry the external task">
        <areavpath v="__TAG__('iw_workarea');"/>
        <successors>
            <successorset description="Try Again">
                <succ v="External Task"/>
            </successorset>
        </successors>
    </usertask>

    <endtask name="End">
    </endtask>

</workflow>
