<?xml version="1.0" standalone="no" ?>
<!DOCTYPE available_templates SYSTEM './available_templates.dtd'>

<!-- 
The require_workarea attribute of the available_templates tag is implicitly set
to true.  That means the workflow templates selection screen will include a
branch/workarea chooser if workarea context is not present during the workflow
instantiation.  If require_workarea attribute is set to false explicitly,
workflow templates selection will ignore any branch/vpath restriction when a
workarea context is not present during workflow instantiation.  Most of the out
of the box workflow template examples require the require_workarea attribute to
be true to work properly.
-->
<available_templates>

    <template_file name='Author Submit Workflow' path='solutions/configurable_author_submit.wft'>
        <allowed>
            <and>
                <command name="submit"/>
                <role name="author"/>
            </and>
        </allowed>
    </template_file>

    <template_file name='Submit Workflow' path='solutions/configurable_default_submit.wft'>
        <allowed>
            <and>
                <command name="submit"/>
                <or>
                    <role name="master"/>
                    <role name="admin"/>
                    <role name="editor"/>
                    <not>
                        <role name="author"/>
                    </not>
                </or>
            </and>
        </allowed>
    </template_file>

    <template_file name='Assignment Workflow' path='solutions/configurable_author_assignment.wft'>
        <allowed>
            <command name="assign"/>
        </allowed>
    </template_file>

    <template_file name='Author Assignment' path='solutions/configurable_author_assignment.wft'>
        <allowed>
            <command name="new_job"/>
        </allowed>
    </template_file>

    <template_file name='Default TFO Submit' path='default/default_TFO_submit.wft'>
        <allowed>
            <command name="tfo_workflow"/>
        </allowed>
    </template_file>

    <template_file name='Author Form Submit' path='default/author_submit_dcr.wft'>
        <allowed>
            <and>
                <or>
                    <command name="tt_data"/>
                    <command name="tt_deletedcr"/>
                </or>
                <or>
                    <role name="author"/>
                    <role name="editor"/>
                </or>
                <not>
                    <role name="admin"/>
                </not>
                <not>
                    <role name="master"/>
                </not>
            </and>
        </allowed>
    </template_file>

</available_templates>
