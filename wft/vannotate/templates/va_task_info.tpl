<iw_comment>
----------------------------------------------------------------------    
va_task_info.tpl

Date/priority/task description for inclusion within VisualAnnotate
email.

PARAMETERS:
    $url_image - Base URL for images
    (usually http://host/iw-cc/vannotate/images)
    $localizer - Resource bundle
    $priority 
    $due_date 
    $task_description

NOTE:  This template is NOT referenced by default in the root 
VisualAnnotate templates (va_mailbody_reviewer.tpl, 
va_mailbody_author.tpl).  You may use this template if your 
workflow contains priority and due date properties.  One such   
workflow is /wft/solutions/configurable_author_assignment.wft. 

To use this template in va_mailbody_*.tpl, you may cut and 
paste the following code, (including the <br>):
 <iw_include pt='va_task_info.tpl' mode='ptlocal'>
	 <![CDATA[
          $iw_param{url_image} = $url_image;
          $iw_param{localizer} = $localizer;
          $iw_param{priority} = $job->GetVariable("priority");
	  $iw_param{due_date} = $job->GetVariable("due_date");
	  $iw_param{task_description} = $task_description;
         ]]>
 </iw_include>
 <br>

NOTE:  This template simply displays the due date and priority it 
is provided with.  It does not attempt to format the due date.
----------------------------------------------------------------------    
</iw_comment>

<iw_perl>
<![CDATA[

	my $url_image = $iw_arg{url_image};
	my $localizer = $iw_arg{localizer};
	my $priority = $iw_arg{priority};
	my $due_date = $iw_arg{due_date};
	my $task_description = $iw_arg{task_description};
	        
        # due date and priority labels
        my $priority_label = $localizer->format("priority");
        my $due_date_label = $localizer->format("due_date");
        
        my $none = $localizer->format("none"); 
        if ($due_date eq "") { $due_date = $none; }
        if ($priority eq "") { $priority = $none; }
     
        # task description label
        my $task_description_label = $localizer->format("task_desc_label");

]]>
</iw_perl>

    <!-- priority and due date --> 
    <table width="100%" border="0" cellpadding="4" cellspacing="0">
      <tr>
      <td class="iw-base-plain-text" nowrap width="1%"><b><iw_value name="$priority_label"/></b></td>
      <td class="iw-base-plain-text"><iw_value name="$priority"/></td>
      </tr>
      <tr>
      <td class="iw-base-plain-text" nowrap><b><iw_value name="$due_date_label"/></b></td>
      <td class="iw-base-plain-text"><iw_value name="$due_date"/></td>
      </tr>
      <tr>
      <td colspan="2" background="<iw_value name="$url_image"/>/bg_divider.gif"><img src="<iw_value name="$url_image"/>/clear.gif" width="1" height="1"></td>
      </tr>
    </table>

    <!-- task description, if one exists -->
    <iw_if expr= ' $task_description ne "" '>
    <iw_then>
    <table width="100%" border="0" cellpadding="4" cellspacing="0">
      <tr>
      <td class="iw-base-plain-text" nowrap width="1%" valign="top"><b><iw_value name="$task_description_label"/></b></td>
      <td class="iw-base-plain-text"><iw_value name="$task_description"/></td>
      </tr>
      <tr>
      <td colspan="2" background="<iw_value name="$url_image"/>/bg_divider.gif"><img src="<iw_value name="$url_image"/>/clear.gif" width="1" height="1"></td>
      </tr>
    </table>
    </iw_then>
    </iw_if>


 

	
