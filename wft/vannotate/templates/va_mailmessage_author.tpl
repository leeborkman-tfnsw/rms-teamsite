<iw_comment>
----------------------------------------------------------------------
va_mailmessage_author.tpl

Welcome note for VisualAnnotate authors.  Consists of "hello" 
greeting, followed by instructions about how to download and 
use VisualAnnotate.

This template is included by va_mailbody_author, the root 
VisualAnnotate author email template.

PARAMETERS:
    $url - Base TeamSite URL (usually http://host/iw/)
----------------------------------------------------------------------
</iw_comment>


<iw_perl>
<![CDATA[
	 my $url = $iw_arg{url};
]]>
</iw_perl>

    <!-- Greeting and message to VisualAnnotate Author -->
    <table width="100%" border="0" cellpadding="3" cellspacing="0">
      <tr>
        <td class="iw-base-plain-text">
        Hello, <b><iw_value name="dcr.workflowinformation.task[1]@owner"/></b>!
        <br><br>
        </td>
      </tr>    
      <tr>
        <td class="iw-base-plain-text">
        One or more reviewers has used <b>VisualAnnotate</b> to review 
        your work. Click on a "View Annotations" link below to view the 
        annotated files.  If you want to install the VisualAnnotate 
        toolbar, <a class="iw-base-plain-link" href="<iw_value name="$url"/>-cc/vainstall">click here</a>.
        </td>
      </tr>
    </table>
      

 

	
