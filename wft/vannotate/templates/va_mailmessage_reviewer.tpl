<iw_comment>
----------------------------------------------------------------------
va_mailmessage_reviewer.tpl

Welcome note for VisualAnnotate reviewers.  Consists of "hello" 
greeting, followed by instructions about how to download and 
use VisualAnnotate.

This template is included by va_mailbody_reviewer.tpl, the root 
VisualAnnotate reviewer email template.

PARAMETERS:
    $url - Base TeamSite URL  (usually http://host/iw/)
----------------------------------------------------------------------
</iw_comment>

<iw_perl>
<![CDATA[
	 my $url = $iw_arg{url};
]]>
</iw_perl>
    
    <!-- Greeting and message to VisualAnnotate Reviewer -->
    <table width="100%" border="0" cellpadding="3" cellspacing="0">
      <tr>
        <td class="iw-base-plain-text">
        Hello, <b><iw_value name="dcr.workflowinformation.task[1]@owner"/></b>!
        <br><br>
        </td>
      </tr>
      <tr>
        <td class="iw-base-plain-text">
        <b><iw_value name="dcr.workflowinformation.task[0]@owner"/></b> 
        has requested that you review some work using 
        <b>VisualAnnotate</b>.  If you are unfamiliar with 
        VisualAnnotate, <a class="iw-base-plain-link" href="<iw_value name="$url"/>-cc/vannotate/tutorial/tutorialLaunch.htm">click here</a>.
        If you want to install the VisualAnnotate toolbar, 
        <a class="iw-base-plain-link" href="<iw_value name="$url"/>-cc/vainstall"> click here</a>.
        </td>
      </tr>
      <tr>            
        <td class="iw-base-plain-text">Click on a "Review and Annotate" link below to 
        begin reviewing with VisualAnnotate.
        </td>
      </tr>
      <tr>
        <td class="iw-base-plain-text">
        If you don't want to install the VisualAnnotate toolbar, or if 
        your browser doesn't support the toolbar, you can use the 
        "Approved" and "Please Revise" links to approve or request 
        revisions of single files or enter comments for 
        <iw_value name="dcr.workflowinformation.task[0]@owner"/> in the
        Comments field.  To approve or request revisions of all listed 
        files at once, click the buttons below the Comments field.
        </td>
      </tr>
    </table>

 

	


