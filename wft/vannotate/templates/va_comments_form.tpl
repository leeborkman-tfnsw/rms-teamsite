<iw_comment>
----------------------------------------------------------------------
va_comments_form.tpl

Textarea (to write task comments) and form buttons 
(to transition task).

Included by va_mailbody_reviewer.tpl (root VisualAnnotate reviewer 
email template).

Note: This template includes a form that posts to the 
      "vatransition" command whenever one of the form buttons
      is pressed.  The following information is posted: 
         taskid  - Task ID of Review task
         comment - Comments typed into textarea (will appear 
                   in the Review History of successive emails).
         transition - Desired task transition (value of 
       "va_approve" and "va_reject" job variables).

PARAMETERS:
    $url_image - Base URL for images
    (usually http://host/iw-cc/vannotate/images)
    $url_web - Base webapp URL 
    (usually http://host/iw-cc/)
    $localizer - Place where we get localized strings
    (resource bundle loaded by va_mailbody_reviewer.tpl)
    $job - This workflow job
----------------------------------------------------------------------
</iw_comment>

<iw_perl>
<![CDATA[

	 my $localizer = $iw_arg{localizer};
	 my $url_web = $iw_arg{url_web};
         my $url_image = $iw_arg{url_image};
         my $target_taskid = $iw_arg{target_taskid};
         my $job = $iw_arg{job};

         my $approve = $job->GetVariable("va_approve");
	 my $reject = $job->GetVariable("va_reject");
        
         # resource strings
  	 my $task_comments_label = 
            $localizer->format("comments_label");	
         my $all_files_label = $localizer->format("all_files");

]]>
</iw_perl>

    <!-- Task Comments textarea and buttons  -->
    <form name="transition_form" action="<iw_value name="$url_web"/>/vatransition" method="post">
    <input type="hidden" name="taskid" value="<iw_value name="$target_taskid"/>">
    
    <!-- This is the heading and the textarea -->     
    <table width="100%" border="0" cellpadding="3" cellspacing="0">
      <tr>
        <td class="iw-base-plain-text" nowrap>
        <b><iw_value name="$task_comments_label"/></b>  
        </td>
      </tr>
      <tr>
        <td>
        <textarea name="comment" rows="4" cols="80" style="width:100%"></textarea>
        </td>
      </tr>
    </table>
      
    <!-- This is the button area -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
	<td>&nbsp;</td>
	<td align="right" valign="middle">
	<table border="0" cellpadding="0" cellspacing="0">
	  <tr>
	    <td class="iw-base-plain-text"><b><iw_value name="$all_files_label"/></b></td>
	    <!-- Spacer -->
	    <td><img src="<iw_value name="$url_image"/>/clear.gif" width="5"></td> 
	    <!-- Button -->
	    <td><input type="submit" name="transition" value="<iw_value name="$approve"/>"></td>
	    <!-- Spacer -->
	    <td><img src="<iw_value name="$url_image"/>/clear.gif" width="5"></td>
	    <!-- Button -->
	    <td><input type="submit" name="transition" value="<iw_value name="$reject"/>"></td>
	  </tr>
	</table>
	</td>
      </tr>
    </table>
    </form>

 

	
