<iw_comment>
----------------------------------------------------------------------
va_mailbody_author.tpl
        
This is the main HTML mail body presentation template for VisualAnnotate 
authors (content contributors).

It is composed of the following included templates:
    va_mailmessage_author.tpl - Welcome message to the author,
    including instructions on how to use and download VisualAnnotate.
    va_file_list.tpl - List of all files in the job, each with a 
    "View Annotations" link.
    va_review_history.tpl - Job history, separated by review cycles, 
    listed in reverse chronological order.

This template is responsible for adding the "html", "head" and "body" 
tags and setting up variables that the included templates use.  In 
addition, the author template provides the html to transition the task 
from the email ("Click here when done").  All other presentation is 
handled by the included templates.

Each included template is its own table, and the master template simply 
snaps the tables together (sometimes separating them by linebreaks).  
You can easily customize the email by removing a template or changing 
the order in which the templates are included.  If you want to add your 
own template, make sure this template includes it (using "iw_include").

NOTE: If this email template is used outside of a VisualAnnotate
review cycle, it will "redirect" to author_iwmailbody.tpl, 
which is located in the iw-home/local/config/wft/solutions 
directory.
----------------------------------------------------------------------
</iw_comment>


<iw_perl>
<![CDATA[
use TeamSite::Config;
use TeamSite::JavaResourceBundle;
use TeamSite::WFtask;
use TeamSite::WFworkflow;

	# construct the base url
	(my $iwhome = TeamSite::Config::iwgethome()) =~ tr|\\|/|;
 	my $iwcfg = "$iwhome/bin/iwconfig iwwebd";
	(my $proto = `$iwcfg default_protocol`) =~ s/\n$//;
	(my $hostname = `$iwcfg host`) =~ s/\n$//;
	(my $port = `$iwcfg ${proto}_port`) =~ s/\n$//;
	my $url = "$proto://" . $hostname;

	# don't bother showing port if it is the default port
	if (($proto eq "http" && ($port != 80 && $port ne "")) 
	    || ($proto eq "https" && ($port != 443 && $port ne ""))) {
	$url .= ":$port";
	}
	
	# add the iw because we're using it all over the place
	$url .= "/iw";

	# some useful urls
	my $url_image = $url . "-cc/vannotate/images";
	my $url_web = $url . "-cc";

        # load the resource bundle that contains localized strings
	my $dir = TeamSite::Config::iwgetlocation("iwconfigs") . "/wft/vannotate/templates";
	my $localizer = TeamSite::JavaResourceBundle->new();
        my $propfile = "vannotate_mail";
        my @bundle = $localizer->load($dir, $propfile); 

        # get workflow and task ids from xml
        my $wfid = iwpt_dcr_value('dcr.workflowinformation.workflow@id');
	my $this_taskid = 
	 iwpt_dcr_value('dcr.workflowinformation.task[0]@id');
	my $target_taskid = 
	 iwpt_dcr_value('dcr.workflowinformation.task[1]@id');

	# use to create workflow and task objects
	my $job = new TeamSite::WFworkflow($wfid);
	my $this_task = new TeamSite::WFtask($this_taskid);
        my $target_task = new TeamSite::WFtask($target_taskid);

	# workflow-related variables
        my $area = $target_task->GetArea();
	$area =~ s|\\|/|g;
        my $approve = $job->GetVariable("va_approve");
	my $reject = $job->GetVariable("va_reject");
	my $current_cycle = $job->GetVariable("cycle");
	my $task_description = $target_task->GetDescription();

        # review urls 
     	my $review_url = "$url_web/review?taskid=$target_taskid";
	my $vannotate_transition_url = 
	 "$url_web/vatransition?taskid=$target_taskid";
	my $task_transition_popup_url = 
         "$url_web/transitiontask?taskid=$target_taskid";

]]>
</iw_perl>

<iw_if expr='!defined($current_cycle)'>
<iw_then>
    <iw_include pt='../../solutions/author_iwmailbody.tpl' mode='ptlocal'></iw_include>
</iw_then>

<iw_else>
<html>
   <head>
      <meta http-equiv=content-type content="text/html; charset=iso-8859-1">
      <!-- This is the stylesheet that the VisualAnnotate web 
           application uses -->
      <link rel="stylesheet" type="text/css" href="<iw_value name="$url_web"/>/base/styles/iw.css">
   </head>
    
   <body class="iw-base-ui-background">
         
      <iw_comment> 
      ------------------------------------------------------------
      Author message.
      Note: In "ptlocal" mode, relative path names are relative to the
            directory in which the current presentation template itself
            is located.  Absolute paths however, are absolute with 
            respect to the filesystem of the machine.
      ------------------------------------------------------------
      </iw_comment>
      <iw_include pt='va_mailmessage_author.tpl' mode='ptlocal'>
         <![CDATA[
          $iw_param{url} = $url;
         ]]>
      </iw_include>
 
      <br>
        
      <iw_comment> 
      -----------------------------------------------------------
      Make changes label and file list
      ----------------------------------------------------------- 
      </iw_comment>
      <iw_include pt='va_file_list.tpl' mode='ptlocal'>
         <![CDATA[
          $iw_param{url_image} = $url_image;
          $iw_param{review_url} = $review_url;
	  $iw_param{vannotate_transition_url} = 
		  $vannotate_transition_url;
          $iw_param{area} = $area;
	  $iw_param{job} = $job;
	  $iw_param{user_type} = "author";
	  $iw_param{heading_label} = 
		  $localizer->format("make_changes_label");
	  $iw_param{review_link_text} = 
		  $localizer->format("view_annotations");
         ]]>
      </iw_include>
          
      <iw_comment> 
      -----------------------------------------------------------
      "Click here when done" task transition area
      
      Note: This link exposes the WebDesk task transition dialog 
	    to the author.  Remove it if you don't want users to 
            interact with WebDesk.
      ----------------------------------------------------------- 
      </iw_comment>
      <!-- Task transition area --> 
      <iw_perl>
      my $click_here_when_done = 
         $localizer->format("click_here_when_done",
                            "$task_transition_popup_url");
      </iw_perl>

      <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td class="iw-base-plain-text"><iw_value name="$click_here_when_done"/></td>
        </tr>
      </table> 
      
      <iw_comment> 
      -----------------------------------------------------------     
      Review history
      ----------------------------------------------------------- 
      </iw_comment>
      <iw_include pt='va_review_history.tpl' mode='ptlocal'>
         <![CDATA[
              $iw_param{localizer} = $localizer;
              $iw_param{url_image} = $url_image;
              $iw_param{review_url} = $review_url;
              $iw_param{area} = $area;
	      $iw_param{job} = $job;
         ]]>
      </iw_include>

   </body>
</html>
</iw_else>
</iw_if>
 

	
