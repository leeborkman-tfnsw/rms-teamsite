<iw_comment>
va_mailheader_author.tpl
        
This is the Mail Header presentation template for VisualAnnotate
authors.  
After it is parsed by the custom tag iwov_map module and merged with 
the task output XML file, the file will follow the DTD 
header_xml.dtd, which is located in the 
iw-home/local/config/wft/solutions directory.

NOTE: If this email template is used outside of a VisualAnnotate
review cycle, it will "redirect" to author_iwmailheader.tpl, 
which is located in the iw-home/local/config/wft/solutions 
directory.

</iw_comment>

<iw_perl>
<![CDATA[
use TeamSite::Config;
use TeamSite::JavaResourceBundle; 
use TeamSite::WFworkflow;        
use TeamSite::CGI_lite;


        # TeamSite installation directory
	(my $iwhome = TeamSite::Config::iwgethome()) =~ tr|\\|/|;
	# load the resource bundle that contains localized strings
	my $dir = TeamSite::Config::iwgetlocation("iwconfigs") . "/wft/vannotate/templates";
	my $localizer = TeamSite::JavaResourceBundle->new();
        my $propfile = "vannotate_mail";
        my @bundle = $localizer->load($dir, $propfile); 
	my $task_description = iwpt_dcr_value('dcr.workflowinformation.workflow.description');
	if ($task_description ne "") { $task_description = "($task_description)"; }
	my $subject_line = $localizer->format("author_subject", $task_description);

	my $wfid = iwpt_dcr_value('dcr.workflowinformation.workflow@id');
	my $job = new TeamSite::WFworkflow($wfid);
        my $current_cycle = $job->GetVariable("cycle");

]]>
</iw_perl>

<iw_if expr='!defined($current_cycle)'>
<iw_then>
   <iw_include pt='../../solutions/author_iwmailheader.tpl' mode='ptlocal'></iw_include>
</iw_then>

<iw_else>
<headers>
    <!-- To the owner of the Author_Work task -->
    <to>
        <iwov_emailmap user='{iw_value name="dcr.workflowinformation.task[1]@owner"/}' />
    </to>
    <!-- From the owner of the email task -->
    <from>
        <iwov_emailmap user='{iw_value name="dcr.workflowinformation.task[0]@owner"/}' />
    </from>
    <!-- Use the subject determined by the properties file and the task description -->
    <subject> 
        <iw_value name="TeamSite::CGI_lite::escape_html_data($subject_line)"/>
    </subject>
</headers>
</iw_else>

</iw_if>
