<iw_comment>
----------------------------------------------------------------------    
va_file_list.tpl
        
List of all files in job with VisualAnnotate-specific links.

Included by va_mailbody_reviewer.tpl and va_mailbody_author.tpl 
(root reviewer and author email templates).

PARAMETERS:
    $url_image - Base URL for images 
    (usually http://host/iw-cc/vannotate/images)
    $review_url - URL to enter review mode 
    (used to construct "Review and Annotate" links)
    $area - Workarea where the files reside
    $job - The workflow job
    $user_type - "reviewer" or "author"
    $vannotate_transition_url - URL to transition files/task 
    (used to construct "Approved" and "Please Revise" links)
    $heading_label - Text that appears above file list 
    (might say "Review Changes" or "Make Changes", depending on 
    user_type)
    $review_link_text - Text for link that enters review mode 
    (might say "Review and Annotate" or "View Annotations", depending on 
    user_type)

NOTE: "Approved" and "Please Revise" may be changed to labels of your 
      choice by changing the values of the "va_approve" and "va_reject" 
      job variables.  Changing these variables will also change the 
      stamp labels on the toolbar.
----------------------------------------------------------------------    
</iw_comment>

<iw_perl>
<![CDATA[
	     
	     
	 my $url_image = $iw_arg{url_image};
	 my $review_url = $iw_arg{review_url};
	 my $area = $iw_arg{area};
	 
	 my $job = $iw_arg{job}; 
	 
	 my $user_type = $iw_arg{user_type};
	 # if user_type = "reviewer" these will be used
	 my $vannotate_transition_url = 
	    $iw_arg{vannotate_transition_url};
	 my $approve = $job->GetVariable("va_approve");
	 my $reject = $job->GetVariable("va_reject");
	 
	 # label for heading and for review link 
         # (different for author/reviewer)           
	 my $heading_label = $iw_arg{heading_label};
	 my $review_link_text = $iw_arg{review_link_text};

         # The location of the TeamSite filesystem mount point.
         (my $iwmnt = TeamSite::Config::iwgetmount()) =~ tr|\\|/|;


#=====================================================================
# url_escape
#
# Returns the escaped string.  Input should be valid UTF-8.
#=====================================================================
sub url_escape
{
    my($text) = @_;
    my($out) = "";
   
    my(@letters) = split('',$text);
    my($val) = 0;
    foreach my $char (@letters) {
	$val = ord($char);
	# Check for A-Z, a-z, 0-9, ., /, and \
	if ($char =~ /[A-Za-z0-9\.\/\\]/) {
	    $out .= $char;
	} elsif ($char eq " ") {
	    $out .= "+";
	} 
	else {
	    # simply url escape it
	    $out .= sprintf("%%%X", $val);
	}
    }
    return $out;
}

	 
 ]]>
</iw_perl>

    <!-- File list heading -->
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tr>
        <td class="iw-base-plain-text" nowrap colspan="2">
        <b><iw_value name="$heading_label"/></b>
        </td>
      </tr>  
  
    <!-- File list -->
    <iw_iterate var='iter' list='dcr.workflowinformation.task[0].files.file'>
    <iw_perl>
    <![CDATA[
	     my $file = iwpt_dcr_value('iter@path') ;
	     $file =~ s|\\|/|g;
             my $url_encoded_vpath = url_escape("$area/$file");

             my $file_exists = -e "$iwmnt$area/$file";
             my $is_dir = -d "$iwmnt$area/$file";
             my $show_review_link = $file_exists && !$is_dir;
	     ]]></iw_perl>
    
      <tr>
        <td width="99%" valign="middle" class="iw-base-plain-text">
        <iw_value name="$file"/>
        </td>
        <td width="1%" align="right" valign="middle" nowrap>

     <iw_if expr=' $show_review_link '>
        <iw_then>
	<img src="<iw_value name="$url_image"/>/icon_bullet_listoption.gif" align="absmiddle">
        <a href="<iw_value name="$review_url"/>&vpath=<iw_value name="$url_encoded_vpath"/>" class="iw-base-plain-link"><iw_value name="$review_link_text"/></a>
	</iw_then>
        <iw_else>&nbsp;</iw_else>
     </iw_if>
     <iw_if expr=' $user_type eq "reviewer" '>
     <iw_then>
        &nbsp;
        <img src="<iw_value name="$url_image"/>/icon_bullet_listoption.gif" align="absmiddle">
        <a href="<iw_value name="$vannotate_transition_url"/>&vpath=<iw_value name="$url_encoded_vpath"/>&transition=<iw_value name="$approve"/>" class="iw-base-plain-link"><iw_value name="$approve"/></a>
        &nbsp;
        <img src="<iw_value name="$url_image"/>/icon_bullet_listoption.gif" align="absmiddle">
        <a href="<iw_value name="$vannotate_transition_url"/>&vpath=<iw_value name="$url_encoded_vpath"/>&transition=<iw_value name="$reject"/>" class="iw-base-plain-link"><iw_value name="$reject"/></a>
    </iw_then>
    </iw_if>
        </td>
      </tr> 
    </iw_iterate>          
 
    </table>
    

 

	
