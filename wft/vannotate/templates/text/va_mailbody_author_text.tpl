<iw_perl><![CDATA[
use TeamSite::Config;
use TeamSite::JavaResourceBundle;
use TeamSite::WFtask;
use TeamSite::WFworkflow;

	# construct the base url
	(my $iwhome = TeamSite::Config::iwgethome()) =~ tr|\\|/|;
 	my $iwcfg = "$iwhome/bin/iwconfig iwwebd";
	(my $proto = `$iwcfg default_protocol`) =~ s/\n$//;
	(my $hostname = `$iwcfg host`) =~ s/\n$//;
	(my $port = `$iwcfg ${proto}_port`) =~ s/\n$//;
	my $url = "$proto://" . $hostname;

	# don't bother showing port if it is the default port
	if (($proto eq "http" && ($port != 80 && $port ne "")) || ($proto eq "https" && ($port != 443 && $port ne ""))) {
	$url .= ":$port";
	}
	
	# add the iw because we're using it all over the place
	$url .= "/iw";

	# shorthand urls
	my $url_image = $url . "-cc/vannotate/images";
	my $url_web = $url . "-cc";

        # load the resource bundle that contains localized strings
	my $dir = TeamSite::Config::iwgetlocation("iwconfigs") . "/wft/vannotate/templates";
	my $localizer = TeamSite::JavaResourceBundle->new();
        my $propfile = "vannotate_mail";
        my @bundle = $localizer->load($dir, $propfile); 

        # get workflow and task ids from xml
        my $wfid = iwpt_dcr_value('dcr.workflowinformation.workflow@id');
	my $this_taskid = iwpt_dcr_value('dcr.workflowinformation.task[0]@id');
	my $target_taskid = iwpt_dcr_value('dcr.workflowinformation.task[1]@id');

	# use to create workflow and task objects
	my $job = new TeamSite::WFworkflow($wfid);
	my $this_task = new TeamSite::WFtask($this_taskid);
        my $target_task = new TeamSite::WFtask($target_taskid);

	# workflow-related variables
        my $area = $target_task->GetArea();
	$area =~ s|\\|/|g;
        my $approve = $job->GetVariable("va_approve");
	my $reject = $job->GetVariable("va_reject");
	my $current_cycle = $job->GetVariable("cycle");
	my $task_description = $target_task->GetDescription();

        # review urls 
     	my $review_url = "$url_web/review?taskid=$target_taskid";
	my $vannotate_transition_url = "$url_web/vatransition?taskid=$target_taskid";
        my $task_transition_popup_url = "$url_web/transitiontask?taskid=$target_taskid";

        my $author_transition_plain =
                 $localizer->format("plain_author_transition_message", "$task_transition_popup_url");]]></iw_perl>
<iw_include pt='va_mailmessage_author_text.tpl' mode='ptlocal'><![CDATA[
               $iw_param{url} = $url;
               ]]></iw_include><iw_include pt='va_file_list_text.tpl' mode='ptlocal'><![CDATA[
              $iw_param{review_url} = $review_url;
	      $iw_param{vannotate_transition_url} = $vannotate_transition_url;
              $iw_param{area} = $area;
	      $iw_param{job} = $job;
	      $iw_param{user_type} = "author";
	      $iw_param{heading_label} = $localizer->format("make_changes_label");
	      $iw_param{review_link_text} = $localizer->format("view_annotations");]]></iw_include>
<iw_value name="$author_transition_plain"/>
<iw_include pt='va_review_history_text.tpl' mode='ptlocal'><![CDATA[
              $iw_param{localizer} = $localizer;
              $iw_param{review_url} = $review_url;
              $iw_param{area} = $area;
	      $iw_param{job} = $job;]]></iw_include>

 

	
