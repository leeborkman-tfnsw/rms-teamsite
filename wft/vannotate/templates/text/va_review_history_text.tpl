<iw_perl><![CDATA[

	my $localizer = $iw_arg{localizer};
	my $review_url = $iw_arg{review_url};
	my $area = $iw_arg{area};

        my $job = $iw_arg{job}; 
	
        my $current_cycle = $job->GetVariable("cycle");
        my $approve = $job->GetVariable("va_approve");
	my $reject = $job->GetVariable("va_reject");

        # job history
        my $job_history_label = $localizer->format("comments_header");
	my $crc_label = $localizer->format("current_review_cycle");
	my $rc_label = $localizer->format("review_cycle");
	my $nothing = $localizer->format("nothing_available");
	my $view_annotations_label = $localizer->format("view_annotations");
	my $task_label = $localizer->format("task");

#=====================================================================
# url_escape
#
# Returns the escaped string.  Input should be valid UTF-8.
#=====================================================================
sub url_escape
{
    my($text) = @_;
    my($out) = "";
   
    my(@letters) = split('',$text);
    my($val) = 0;
    foreach my $char (@letters) {
	$val = ord($char);
	# Check for A-Z, a-z, 0-9, ., /, and \
	if ($char =~ /[A-Za-z0-9\.\/\\]/) {
	    $out .= $char;
	} elsif ($char eq " ") {
	    $out .= "+";
	} 
	else {
	    # simply url escape it
	    $out .= sprintf("%%%X", $val);
	}
    }
    return $out;
}
]]></iw_perl>
<iw_value name="$job_history_label"/>
------------------------------------
<iw_value name="$crc_label"/>
------------------------------------<iw_perl><![CDATA[
	     # will be set to 0 if we see writable comments in a review cycle
	     my $write_nothing_available_comment = "1";
]]></iw_perl><iw_iterate var='iter' list='dcr.workflowinformation.task[0].comments.comment' order='reverse'><iw_perl><![CDATA[
		  my $date = iwpt_dcr_value('iter@date');
		  my $realdate = localtime($date);
                  my $taskid = iwpt_dcr_value('iter@task');
		  my $user = iwpt_dcr_value('iter@user');

		  my $whole_comment = iwpt_dcr_value('iter');

		  my @file_transition_comments = $whole_comment =~ /FILECOMMENT:(.*)/gx;

		  my $comment_type = "defaultcomment";
		  if ($user eq "SYSTEM" || !$whole_comment) { $comment_type = "ignoredcomment"; }
			 
		  # check for filecomment or taskcomment by parsing out comment
		  my $file_name = "";
		  my $file_transition = "";
		  my $file_snapshot = "";
                  my $task_comment_id = "";
		  my $task_transition = "";
		  my $task_comment = "";
		 		
                  if ($comment_type ne "ignoredcomment") {
 
		      ($file_name, $file_transition, $file_snapshot) = $whole_comment =~ /FILE:\s+(.*)\s+TRANSITION:\s+(.*)\s+SNAPSHOT:\s+(.*)/g;
		      if (defined($file_name) && ($file_name ne "")) { $comment_type = "filecomment"; }
    
		      if ($comment_type ne "filecomment") {
			  ($task_comment_id, $task_transition, $task_comment) = $whole_comment =~ /TASK:\s+(.*)\s+TRANSITION:\s+(.*)\s+COMMENTS:\s+(.*)/gs;
		      }
		      if (defined($task_comment_id) && ($task_comment_id ne "")) { $comment_type = "taskcomment"; }
	          }
		  
		  # check for cyclecomment by parsing out comment
		  my $cycle_num = "";
		  my $this_cycle_label = $crc_label;
		  ($cycle_num) = $whole_comment =~ /CYCLE:\s+(\d+)/;
		  if (defined($cycle_num) && $cycle_num ne "") { 
			  $cycle_num = $1;
                          if ($cycle_num ne "1")
                          {
                              $comment_type = "cyclecomment";
			      $cycle_num = $cycle_num - 1;
			      if ($cycle_num ne $current_cycle)
			      {
				  $this_cycle_label = "$rc_label $cycle_num";
			      }
			  } else { $comment_type = "ignoredcomment" }

		  }
		  
		  # if it's one of these types, set a flag saying
		  # we shouldn't write the nothing available string
		  if ( ($comment_type eq "filecomment") || 
		       ($comment_type eq "taskcomment") || 
		       ($comment_type eq "defaultcomment") )
		  {
		      $write_nothing_available_comment = "0";
		  }

		  # remember it so that cyclecomment can access it
		  my $write_na = $write_nothing_available_comment;
		  # reset it for the next time around
		  if ($comment_type eq "cyclecomment") { $write_nothing_available_comment = "1"; }
		  
]]></iw_perl><iw_if expr=' $whole_comment '><iw_then><iw_if expr=' ($comment_type ne "cyclecomment") &amp;&amp; ($comment_type ne "ignoredcomment") '><iw_then>
<iw_value name="$user"/>  <iw_value name="$realdate"/>:
</iw_then></iw_if><iw_ifcase name='$comment_type'><iw_case value="filecomment"><iw_iterate var='fc' list='@file_transition_comments'><iw_perl><![CDATA[
	      	 ($file_name, $file_transition, $file_snapshot) = $fc =~ /FILE:\s+(.*)\s+TRANSITION:\s+(.*)\s+SNAPSHOT:\s+(.*)/g;
		 if ($file_transition eq "no_transition") { $file_transition = $localizer->format("no_transition");} 
                 my $url_encoded_vpath = url_escape("$area/$file_name");]]></iw_perl><iw_value name="$file_transition"/>: <iw_value name="$file_name"/><iw_if expr=' $file_snapshot ne "no_snapshot" '><iw_then>
<iw_value name="$view_annotations_label"/>:  <iw_value name="$review_url"/>&vpath=<iw_value name="$url_encoded_vpath"/>&snapshotid=<iw_value name="$file_snapshot"/></iw_then></iw_if>
</iw_iterate></iw_case>
<iw_case value="taskcomment"><iw_value name="$task_transition"/>: <iw_value name="$task_label"/> <iw_value name="$task_comment_id"/><iw_if expr=' ($task_comment ne "") &amp;&amp; ($task_comment ne "none") '><iw_then>(<iw_value name="$task_comment"/>)</iw_then></iw_if></iw_case> 
<iw_case value="cyclecomment"><iw_if expr=' $write_na eq "1" '><iw_then>
(<iw_value name="$nothing"/>)</iw_then></iw_if>   
------------------------------------
<iw_value name="$this_cycle_label"/>
------------------------------------</iw_case><iw_case value="defaultcomment"><iw_value name="$whole_comment"/></iw_case></iw_ifcase></iw_then></iw_if></iw_iterate>
<iw_if expr=' $write_nothing_available_comment eq "1" '><iw_then>(<iw_value name="$nothing"/>)</iw_then></iw_if>   

 

	

