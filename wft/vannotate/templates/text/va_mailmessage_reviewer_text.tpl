<iw_perl><![CDATA[
	 my $url = $iw_arg{url};
]]></iw_perl>
Hello, <iw_value name="dcr.workflowinformation.task[1]@owner"/>!

<iw_value name="dcr.workflowinformation.task[0]@owner"/> has requested that you review some work using VisualAnnotate.  If you are unfamiliar with VisualAnnotate, go to 
<iw_value name="$url"/>-cc/vannotate/tutorial/tutorialLaunch.htm.  
If you want to install the VisualAnnotate toolbar, go to 
<iw_value name="$url"/>-cc/vainstall.
