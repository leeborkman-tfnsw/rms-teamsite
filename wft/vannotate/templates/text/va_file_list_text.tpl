<iw_perl><![CDATA[
	
	my $review_url = $iw_arg{review_url};
	my $area = $iw_arg{area};
       
        my $job = $iw_arg{job}; 

	my $user_type = $iw_arg{user_type};
   
        # label for heading and for review link (different for author/reviewer)    
        my $heading_label = $iw_arg{heading_label};
	my $review_link_text = $iw_arg{review_link_text};
  
#=====================================================================
# url_escape
#
# Returns the escaped string.  Input should be valid UTF-8.
#=====================================================================
sub url_escape
{
    my($text) = @_;
    my($out) = "";
   
    my(@letters) = split('',$text);
    my($val) = 0;
    foreach my $char (@letters) {
	$val = ord($char);
	# Check for A-Z, a-z, 0-9, ., /, and \
	if ($char =~ /[A-Za-z0-9\.\/\\]/) {
	    $out .= $char;
	} elsif ($char eq " ") {
	    $out .= "+";
	} 
	else {
	    # simply url escape it
	    $out .= sprintf("%%%X", $val);
	}
    }
    return $out;
}
]]></iw_perl>
<iw_value name="$heading_label"/><iw_iterate var='iter' list='dcr.workflowinformation.task[0].files.file'><iw_perl><![CDATA[
my $file = iwpt_dcr_value('iter@path') ;
$file =~ s|\\|/|g;
my $url_encoded_vpath = url_escape("$area/$file");
]]></iw_perl>
<iw_value name="$file"/>
<iw_value name="$review_link_text"/>:  <iw_value name="$review_url"/>&vpath=<iw_value name="$url_encoded_vpath"/></iw_iterate>          



