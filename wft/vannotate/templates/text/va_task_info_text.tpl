<iw_perl><![CDATA[

	my $localizer = $iw_arg{localizer};
	my $priority = $iw_arg{priority};
	my $due_date = $iw_arg{due_date};
	my $task_description = $iw_arg{task_description};
	        
        # due date and priority labels
        my $priority_label = $localizer->format("priority");
        my $due_date_label = $localizer->format("due_date");
        
        my $none = $localizer->format("none"); 
        if ($due_date eq "") { $due_date = $none; }
        if ($priority eq "") { $priority = $none; }
     
        # task description label
        my $task_description_label = $localizer->format("task_desc_label");

]]></iw_perl>
<iw_value name="$priority_label"/>  <iw_value name="$priority"/>
<iw_value name="$due_date_label"/>  <iw_value name="$due_date"/><iw_if expr= ' $task_description ne "" '><iw_then>
<iw_value name="$task_description_label"/>  <iw_value name="$task_description"/></iw_then></iw_if>
