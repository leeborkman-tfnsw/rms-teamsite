<iw_comment>
File: <iw-home>/local/config/wft/solutions/ccmail_textbody.tpl
        
This is a Text Mail Body presentation template for use with Content Center.
It can be used with iw_solution_email.ipl to create the text body of an email
message.

This template is applied to an XML document that contains information
regarding a workflow job and tasks within that job.  The output of the
template is a text document.

We assume that:
  dcr.workflowinformation.task[0] is the email task itself, and
  dcr.workflowinformation.task[1] is the subject task, which could
    be a user task or a group task.

ARGUMENTS:
    type_of_task - The type of the subject task.  One of the following
                   values: 'author' or 'review'.

</iw_comment><iw_perl>
<![CDATA[

use TeamSite::CGI_lite;
use TeamSite::Config;
use TeamSite::IW_cfg;
use TeamSite::I18N_utils;
use TeamSite::JavaResourceBundle;
use Text::Wrap;
use POSIX qw(locale_h strftime);

my $type_of_task = $iw_arg{type_of_task} || 'author';

# TeamSite installation directory
(my $iwhome = TeamSite::Config::iwgethome()) =~ tr|\\|/|;

# The location of the TeamSite filesystem mount point.
(my $iwmnt = TeamSite::Config::iwgetmount()) =~ tr|\\|/|;

# The location of the TeamSite config directory.
(my $iwconfigs = TeamSite::Config::iwgetlocation("iwconfigs")) =~ tr|\\|/|;

# Get locale of server and use that info to read in appropriate properties file
my $i_will_change_iw_cfg = 0;
my $cfg = TeamSite::IW_cfg->new();
$cfg->read( $i_will_change_iw_cfg );
my $iwserver_locale = $cfg->lookup( 'iwserver', 'server_locale');
$iwserver_locale =~ s/#.*$//;     # remove trailing comments, if any
$iwserver_locale =~ s/[;\s]*$//;  # remove trailing semicolon/whitespace, if any
my $result_lang = TeamSite::I18N_utils::iw_locale_2_lang($iwserver_locale);

# Load the appropriate resource bundle for localization.
my $propfile = "ccmail" . $result_lang;
my $localizer = TeamSite::JavaResourceBundle->new();
my $template_dir = "$iwconfigs/wft/solutions";
my @bundle = $localizer->load($template_dir, $propfile); 

my $base_url = get_base_url();

my $email_task = iwpt_dcr_list('dcr.workflowinformation.task')[0];
my $user_task = iwpt_dcr_list('dcr.workflowinformation.task')[1];
my $is_group_task = (iwpt_dcr_value('user_task@owner') eq '<no user>');
my $is_readonly_task = (iwpt_dcr_value('user_task@readonly') eq 't');
my $is_review_task = $type_of_task eq 'review';

my $areapath = iwpt_dcr_value('email_task.areavpath@v');
$areapath =~ s|\\|/|g;
my $staging = "$areapath";
$staging =~ s/\/WORKAREA\/[^\/]+/\/STAGING/;

my @task_variables = iwpt_dcr_list('user_task.variables.variable');
my @job_variables = iwpt_dcr_list('dcr.workflowinformation.workflow.variables.variable');
# Show the most recent comment first
my @comments = reverse iwpt_dcr_list('email_task.comments.comment');

my $greeting = undef;
if ($is_review_task) {
  $greeting = $localizer->format('approval.greeting');
} elsif ($is_group_task ) {
  $greeting = $localizer->format('edit_group.greeting');
} else {
  $greeting = $localizer->format('edit.greeting');
}

# Format the current date like Dec 8, 2002
# NOTE: Normally, the user task will not yet be active, so we just
#       use the current time.
my $task_activation = POSIX::strftime($localizer->format("dateformat"), 
                                      localtime());
# Format date like 8/24/2001 2:58 PM EST
# XYZZY: The century is not known!
my $job_activation = POSIX::strftime($localizer->format("datetimeformat"),
                                     localtime(iwpt_dcr_value('dcr.workflowinformation.workflow@activationtime')));

my $areavpath = iwpt_dcr_value('user_task.areavpath@v');
my $workarea_name = "$areavpath";
$workarea_name =~ s/^.+WORKAREA[\/\\](.+)$/$1/;
# For the project name, use the last segment of the branch name.
my $project_name = "$areavpath";
if ($project_name =~ m/([^\/\\]+)[\/\\](WORKAREA|STAGING)/) {
    $project_name = $1;
}

# Convert the priority from an integer into its corresponding label.
my $priority = get_variable("priority", @task_variables)
               || get_variable("priority", @job_variables);
if ( ! defined $priority ) {
  $priority = "(none)";
} else {
  my $label = $localizer->format("map.priority.$priority.label");
  $priority = $label unless $label eq "";
}

my $due_date = get_variable("due_date", @task_variables)
               || get_variable("due_date", @job_variables);
if ($due_date) {
  # Convert the date from a time(2) value in seconds since the Epoch
  # (Midnight, January 1, 1970) to MM/DD/YYYY.
  $due_date = strftime($localizer->format("dateformat"), localtime($due_date));
} else {
  $due_date = "(none)";
}

# Give the key for a string resource, return the uppercased, localized string
sub local_upcase
{
    my ($key) = @_;
    return uc $localizer->format($key);
}

# Determine the base URL for the TeamSite server, with the protocol,
# server name and (optionally) the port number.
sub get_base_url
{
    # get the web daemon information
    my ($iwcfg) = "$iwhome/bin/iwconfig iwwebd";

    # using HTTP or HTTPS
    (my $proto = `$iwcfg default_protocol`) =~ s/\n$//;

    # get the host name
    (my $hostname = `$iwcfg host`) =~ s/\n$//;

    # get the port number
    (my $port = `$iwcfg ${proto}_port`) =~ s/\n$//;
    my($url) = "$proto://" . $hostname;

    # don't bother showing port if it is the default port
    if (($proto eq "http" && ($port != 80 && $port ne ""))
        || ($proto eq "https" && ($port != 443 && $port ne ""))) {
        $url .= ":$port";
    }

    return $url;
}

sub get_variable
{
    my ($key, @variable_nodes) = @_;

    for (my $i = 0; $i < scalar(@variable_nodes); $i++) {
        my $variable_node = $variable_nodes[$i];
	my $this_key = $variable_node->value('@key');
        if ( $key eq $this_key ) {
            return $variable_node->value('@value');
        }
    }
    return ''; # Not found
}

# Build the text body of the message
sub get_body_text
{
    my $page_width = 72;
    local ($Text::Wrap::columns = $page_width);
    my $initial_tab = "  "; # Indent 2 spaces
    my $subsequent_tab = "  "; # Also 2 spaces

    my $result = "$greeting\n"
        . $initial_tab . $base_url . '/iw-cc/viewtaskdetails?taskid=' . iwpt_dcr_value('user_task@id') . "\n\n";

    # Write these values into three columns
    my @name_value_info =
        (
         ['project', "$project_name: $workarea_name"],
         ['taskdetails.owner',
          (iwpt_dcr_value('user_task@owner') eq '<no user>'
           ? $localizer->format('unassigned')
           : iwpt_dcr_value('user_task@owner'))],
         ['taskdetails.activationdate', $task_activation],
         ['taskdetails.priority', $priority],
         ['taskdetails.duedate', $due_date],
         );
    my $column_width = ($page_width / 3) - 1;
    my $column = 0;
    for my $pair (@name_value_info) {
        my $text = local_upcase($pair->[0]) . " " . $pair->[1];
        if (length($text) >= $column_width) {
            # Don't chop the text so that it fits within the column
            #$text = substr($text, 0, $column_width);
        } else {
            # pad the text with spaces so that it fills the column
            $text .= ' ' x ($column_width - length($text));
        }
        $result .= $text;
        if ($column == 2) {
            # Start a new row
            $result .= "\n\n";
            $column = 0;
        } else {
            $result .= " ";
            $column++;
        }
    }
    $result .= "\n\n" if $column > 0;

    $result .= join("\n",
                    "",
                    local_upcase('taskdetails.descriptioncomments'),
                    '-' x $page_width,
                    "",
                    local_upcase('taskdetails.description'),
                    wrap($initial_tab, $subsequent_tab, iwpt_dcr_value('dcr.workflowinformation.workflow.description') . " [" . iwpt_dcr_value('dcr.workflowinformation.workflow@creator') . ": " . $job_activation . "]"),
                    "",
                    local_upcase('taskdetails.currenttask'),
                    wrap($initial_tab, $subsequent_tab, iwpt_dcr_value('user_task.description')),
                    "",
                    local_upcase('comments')) . "\n";

    if (scalar(@comments) > 0)
    {
        for my $iter (@comments) {
            $result .= wrap($initial_tab, $subsequent_tab,
                            iwpt_dcr_value('iter') . " [Task "
                            . iwpt_dcr_value('iter@task') . ": "
                            . iwpt_dcr_value('iter@user') . " "
                            . POSIX::strftime($localizer->format("datetimeformat"), localtime(iwpt_dcr_value('iter@date'))) . "]")
                . "\n\n";
        }
    }
    else
    {
        $result .= $initial_tab . "--\n\n";
    }

    $result .= local_upcase('taskdetails.attachedfiles') . "\n"
        . '-' x $page_width . "\n\n";

    if (scalar(iwpt_dcr_list('email_task.files.file')) > 0)
    {
        for my $iter (iwpt_dcr_list('email_task.files.file'))
        {
            my $file = iwpt_dcr_value('iter@path');
            $file =~ s|\\|/|g;
            my $deleted = iwpt_dcr_value('iter@deleted') eq "t";
            # vpath to file
            my $file_vpath = "$areapath/$file";
            # Full OS path to the submitted file
            my $file_in_workarea_utf8 = "$iwmnt$file_vpath";
            # Full OS path to the same file in STAGING.
            my $file_in_staging_utf8 = "$iwmnt$staging/$file";
            my $file_in_workarea_local = $file_in_workarea_utf8;
            my $file_in_staging_local  = $file_in_staging_utf8;
            if ($iwserver_locale ne '') {
                my $enc = 
                 TeamSite::I18N_utils::iw_locale_2_encoding($iwserver_locale);
                $enc = TeamSite::I18N_utils::promote_encoding( $enc );
                $file_in_workarea_local =
                   TeamSite::I18N_utils::utf8_to_other_encoding($enc,
                                              $file_in_workarea_utf8);
                $file_in_staging_local =
                   TeamSite::I18N_utils::utf8_to_other_encoding($enc,
                                              $file_in_staging_utf8);
            }
            my $new = ! -e $file_in_staging_local;

            # Determine the icon to use for this file
            my $file_icon = "";
            if ($deleted) {
                $file_icon .= "[DELETE]";
            } elsif (-d $file_in_workarea_local) {
                $file_icon .= "[Folder]";
            } elsif ($new) {
                $file_icon .= "[NEW]";
            } else {
                $file_icon .= ""; # Simply modified
            }
            $result .= iwpt_dcr_value('iter@path');
            if ( length($file) < 50 ) {
                $result .= ' ' x (50 - length($file)); # Pad on the right
            }
            $result .= " $file_icon\n";
        }
    }
    else
    {
        $result .= $localizer->format('taskdetails.nofiles') . "\n";
    }

    return $result;
}

]]>
</iw_perl>
<iw_value name="get_body_text()"/>

