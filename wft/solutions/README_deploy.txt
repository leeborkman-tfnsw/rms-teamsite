OOTB WorkFlow intergration with OD  
------------------------------------------------------

The OD components for this integration are wft_opendeploy.xml,
wft_opendeploy.ipl and wft_opendeploy.cfg. 

These files are present in iwhome/local/config/wft/solutions.

Following steps are required:
-------------------------------------------
* wft_opendeploy.xml is the deployment config file. Copy this 
  file to the <iwodhome>/conf directory.

* wft_opendeploy.ipl is the perl script to kick off the deployment.

* wft_opendeploy.cfg is the opendeploy configuration file which 
   provides a mapping between the branch name and corresponding 
   deployment attributes required for deployment. Each deployment 
   mapping contains three required items, delimited  by commas. 
   This file provides a mapping between the branch name and the 
   corresponding deployment attributes required for deployment. 
   The deployment attributes are  node name  and the target 
   directory. 

   The fields that is required for this file are the following:
    * BRANCH NAME:
         This is the name of the source branch. 
    * DESTINATION NODE NAME:
         The logical node name of the OpenDeploy receiver that is
         configured in the OpenDeploy Base Server's
         <iwodhome>/etc/odnodes.xml. 
    * DESTINATION PATH:
         Path on the destination server where the content will be
         deployed. This path must be included as an allowed directory
         in the OpenDeploy receiver's <iwodhome>/etc/odbase.xml or
         <iwodhome>/etc/odrcvr.xml configuration file. 
