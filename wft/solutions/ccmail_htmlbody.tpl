<iw_comment>
File: <iw-home>/local/config/wft/solutions/ccmail_htmlbody.tpl
        
This is an HTML Mail Body presentation template for use with Content Center.
It can be used with iw_solution_email.ipl to create the HTML body of an email
message.

This template is applied to an XML document that contains information
regarding a workflow job and tasks within that job.  The output of the
template is an HTML document.

We assume that:
  dcr.workflowinformation.task[0] is the email task itself, and
  dcr.workflowinformation.task[1] is the subject task, which could
    be a user task or a group task.

ARGUMENTS:
    type_of_task - The type of the subject task.  One of the following
                   values: 'author' or 'review'.

</iw_comment>

<iw_perl>
<![CDATA[

if ( $] ge "5.008") { require Encode; }
use TeamSite::CGI_lite;
use TeamSite::Config;
use TeamSite::IW_cfg;
use TeamSite::I18N_utils;
use TeamSite::JavaResourceBundle;
use POSIX qw(locale_h strftime);
use URI;

my $type_of_task = $iw_arg{type_of_task} || 'author';

# TeamSite installation directory
(my $iwhome = TeamSite::Config::iwgethome()) =~ tr|\\|/|;

# The location of the TeamSite filesystem mount point.
(my $iwmnt = TeamSite::Config::iwgetmount()) =~ tr|\\|/|;

# The location of the TeamSite config directory.
(my $iwconfigs = TeamSite::Config::iwgetlocation("iwconfigs")) =~ tr|\\|/|;

# Get locale of server and use that info to read in appropriate properties file
my $i_will_change_iw_cfg = 0;
my $cfg = TeamSite::IW_cfg->new();
$cfg->read( $i_will_change_iw_cfg );
my $iwserver_locale = $cfg->lookup( 'iwserver', 'server_locale');
$iwserver_locale =~ s/#.*$//;     # remove trailing comments, if any
$iwserver_locale =~ s/[;\s]*$//;  # remove trailing semicolon/whitespace, if any
my $result_lang = TeamSite::I18N_utils::iw_locale_2_lang($iwserver_locale);

# Load the appropriate resource bundle for localization.
my $propfile = "ccmail" . $result_lang;
# print "<H2>Using properties file : $propfile</H2>";
my $localizer = TeamSite::JavaResourceBundle->new();
my $template_dir = "$iwconfigs/wft/solutions";
my @bundle = $localizer->load($template_dir, $propfile); 

my $base_url = get_base_url();

my $email_task = iwpt_dcr_list('dcr.workflowinformation.task')[0];
my $user_task = iwpt_dcr_list('dcr.workflowinformation.task')[1];
my $is_group_task = (iwpt_dcr_value('user_task@owner') eq '<no user>');
my $is_readonly_task = (iwpt_dcr_value('user_task@readonly') eq 't');
my $is_review_task = $type_of_task eq 'review';

# Determine the URL for the help link.
# We do not have distinct pages for group and user tasks.

if ($result_lang) {
	$result_lang =~ s/^_//;	# remove preceeding underscore
} else {
	$result_lang = "en";	# default
}
my $help_url = "/iw-cc/ccstd/help/" . $result_lang . "/";

if ($is_review_task && $is_group_task) {
  $help_url .= "s_email_approve.html";
} elsif ($is_review_task ) {
  $help_url .= "s_email_approve.html";
} elsif ($is_group_task ) {
  $help_url .= "s_email_edit.html";
} else {
  $help_url .= "s_email_edit.html";
}

my $areapath = iwpt_dcr_value('email_task.areavpath@v');
$areapath =~ s|\\|/|g;

# Convert the areapath to utf-8 octets
my $areapath_utf8 = $areapath;
if ( $] ge "5.008") { $areapath_utf8 = Encode::encode_utf8($areapath) }

my $staging = "$areapath";
$staging =~ s/\/WORKAREA\/[^\/]+/\/STAGING/;

my @task_variables = iwpt_dcr_list('user_task.variables.variable');
my @job_variables = iwpt_dcr_list('dcr.workflowinformation.workflow.variables.variable');
my @comments = iwpt_dcr_list('email_task.comments.comment');
my $last_comment = (@comments ? $comments[$#comments] : undef);

# Format the current date like 12/8/02
# NOTE: Normally, the user task will not yet be active, so we just
#       use the current time.
my $task_activation = POSIX::strftime($localizer->format("dateformat"), 
                                      localtime());
# Format date like 8/24/01 2:58 PM EST
my $job_activation = POSIX::strftime($localizer->format("datetimeformat"),
                                     localtime(iwpt_dcr_value('dcr.workflowinformation.workflow@activationtime')));

my $areavpath = iwpt_dcr_value('user_task.areavpath@v');
my $workarea_name = "$areavpath";
$workarea_name =~ s/^.+WORKAREA[\/\\](.+)$/$1/;
# For the project name, use the last segment of the branch name.
my $project_name = "$areavpath";
if ($project_name =~ m/([^\/\\]+)[\/\\](WORKAREA|STAGING)/) {
    $project_name = $1;
}

# Convert the priority from an integer into its corresponding label.
my $priority = get_variable("priority", @task_variables)
               || get_variable("priority", @job_variables);
if ( ! defined $priority ) {
  $priority = "(none)";
} else {
  my $label = $localizer->format("map.priority.$priority.label");
  $priority = $label unless $label eq "";
}

my $due_date = get_variable("due_date", @task_variables)
               || get_variable("due_date", @job_variables);
if ($due_date) {
  # Convert the date from a time(2) value in seconds since the Epoch
  # (Midnight, January 1, 1970) to MM/DD/YY.
  $due_date = strftime($localizer->format("dateformat"), localtime($due_date));
} else {
  $due_date = "(none)";
}

my $row_odd = 1;

# List of actions that apply to "Attached Files".
my @filelist_actions = (
    { label_id => 'taskdetails.newform',
      url => '/iw-cc/addnewformtotask',
      icon => '/iw-cc/teamsite/images/command_icons/icn_add.gif',
    },
    { label_id => 'taskdetails.import',
      url => '/iw-cc/importfiletotask',
      icon => '/iw-cc/teamsite/images/command_icons/icn_import.gif',
    },
    { label_id => 'taskdetails.modifiedfiles',
      url => '/iw-cc/attachmodifiedfiletotask',
      icon => '/iw-cc/base/images/icn_doc.gif',
    },
    { label_id => 'taskdetails.existingfiles',
      url => '/iw-cc/attachexistingfiletotask',
      icon => '/iw-cc/base/images/icn_doc.gif',
    },
  );

# List of actions that apply to individual attached files.
my @file_actions = (
    { name => 'edit',
      label_id => 'taskdetails.file.edit',
      url => '/iw-cc/edit',
    },
    { name => 'preview',
      label_id => 'taskdetails.file.preview',
      url => '/iw-cc/previewfile',
    },
    { name => 'vdiff',
      label_id => 'taskdetails.file.vdiff',
      url => '/iw-cc/viewdiff',
    },
    { name => 'properties',
      label_id => 'taskdetails.file.properties',
      url => '/iw-cc/properties',
    },
    { name => 'comments',
      label_id => 'taskdetails.file.comments',
      url => '/iw-cc/taskfilecomment',
    },
  );

# List of actions that apply to individual attached folders.
my @folder_actions = (
    { name => 'properties',
      label_id => 'taskdetails.file.properties',
      url => '/iw-cc/properties',
    },
    { name => 'comments',
      label_id => 'taskdetails.file.comments',
      url => '/iw-cc/taskfilecomment',
    },
  );

# Returns a table of filelist actions as HTML.
sub filelist_actions_html ()
{
    my $result = '<table cellspacing="0" cellpadding="0" border="0">' . "\n"
        . '<tr>' . "\n";
    my $first_action = 1;
    for my $action (@filelist_actions) {
        my $label = html_escape($localizer->format($action->{label_id}));
        unless ($first_action) {
            $result .= '<td><img src="/iw-cc/base/images/div_optionbar.gif"/></td>' . "\n";
        }
        my $uri = URI->new($action->{url});
        $uri->query_form("taskid" => iwpt_dcr_value('user_task@id'));
        $result .= '<td nowrap>' . "\n"
            . '<a href="' . $uri . '" title="'
            . $label . '" class="iw-base-link">'
            . '<img alt="[icon]" src="' . $action->{icon}
            . '" class="iw-base-link-icon"/> '
            . $label . '</a>' . "\n"
            . '</td>' . "\n";
        $first_action = 0;
    }
    $result .= '</tr>' . "\n" . '</table>' . "\n";
    return $result;
}

# Returns a table of file actions as HTML.
# List all attached files with the following per-file action links:
#  Edit (unless the user task is readonly)
#  Preview (unless files was deleted; not in workarea)
#  View Differences (unless new or deleted file)
#  Properties
#  File Comments
sub file_actions_html
{
    my ($file_vpath, $isNew, $deleted, $isFolder, $num_comments) = @_;

    # Convert the file_vpath to utf-8 octets
    if ( $] ge "5.008") { $file_vpath = Encode::encode_utf8($file_vpath) }
    
    my $result = '<table cellspacing="0" cellpadding="0" border="0">' . "\n"
        . '<tr>' . "\n";
    my $first_action = 1;
    for my $action ($isFolder ? @folder_actions : @file_actions) {
        my $label = html_escape($localizer->format($action->{label_id}));

        # The edit action should not appear on readonly tasks.
        next if $action->{name} eq 'edit' && $is_readonly_task;
        next if $action->{name} eq 'preview' && $deleted;
        next if $action->{name} eq 'vdiff' && ($deleted || $isNew);

        # Add an asterix to the file comments label if there are any comments
        if ($action->{name} eq 'comments' && ($num_comments > 0)) {
            $label .= "*";
        }

        my $uri = URI->new($action->{url});
        if ($action->{name} eq 'comments') {
            $uri->query_form("vpath" => $file_vpath,
                             "taskid" => iwpt_dcr_value('user_task@id'));
        } else {
            $uri->query_form("vpath" => $file_vpath);
        }

        unless ($first_action) {
            $result .= '<td><img src="/iw-cc/base/images/div_optionbar.gif"/></td>' . "\n";
        }
        $result .= '<td nowrap>' . "\n"
            . '<a href="' . $uri . '" title="'
            . $label . '" class="iw-base-actionlist-link">'
            . $label . '</a>' . "\n"
            . '</td>' . "\n";
        $first_action = 0;
    }
    $result .= '</tr>' . "\n" . '</table>' . "\n";
    return $result;
}

# Determine the base URL for the TeamSite server, with the protocol,
# server name and (optionally) the port number.
sub get_base_url
{
    # get the web daemon information
    my ($iwcfg) = "$iwhome/bin/iwconfig iwwebd";

    # using HTTP or HTTPS
    (my $proto = `$iwcfg default_protocol`) =~ s/\n$//;

    # get the host name
    (my $hostname = `$iwcfg host`) =~ s/\n$//;

    # get the port number
    (my $port = `$iwcfg ${proto}_port`) =~ s/\n$//;
    my($url) = "$proto://" . $hostname;

    # don't bother showing port if it is the default port
    if (($proto eq "http" && ($port != 80 && $port ne ""))
        || ($proto eq "https" && ($port != 443 && $port ne ""))) {
        $url .= ":$port";
    }

    return $url;
}

sub get_variable
{
    my ($key, @variable_nodes) = @_;

    for (my $i = 0; $i < scalar(@variable_nodes); $i++) {
        my $variable_node = $variable_nodes[$i];
	my $this_key = $variable_node->value('@key');
        if ( $key eq $this_key ) {
            return $variable_node->value('@value');
        }
    }
    return ''; # Not found
}

sub html_escape
{
    return TeamSite::CGI_lite::escape_html_data(shift);
}

]]>
</iw_perl>


<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<base href="<iw_value name="$base_url"/>/">
<link rel="stylesheet" type="text/css" href="/iw-cc/base/styles/iw.css">
<link rel="stylesheet" type="text/css" href="/iw-cc/base/styles/custom.css">
<style>
body {
    margin:4;
}
</style>
</head>

<body class="iw-base-background">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td nowrap>
      <!-- crumbtrail -->
      <a href="/iw-cc/viewtasklist"
         class="iw-base-crumbtrail-link"><iw_value name="($localizer->format('workflow.label'))"/></a>
      <span class="iw-base-crumbtrail-separator" style="">&gt;</span>
      <a href="/iw-cc/viewtaskdetails?taskid=<iw_value name="user_task@id"/>"
         class="iw-base-crumbtrail-link"><iw_value name="($localizer->format('taskdetails.task', iwpt_dcr_value('user_task@id')))"/></a>
    </td>
    <td nowrap align="right">
      <a href="<iw_url_escape><iw_value name="$help_url"/></iw_url_escape>" class="iw-base-actionlist-link"><iw_value name="($localizer->format('help.label'))"/></a>
    </td>
  </tr>
</table>

<!--Spacer-->
<div><img src="/iw-cc/base/images/clear.gif" alt="Spacer" width="10" height="5"></div>
<!--Horizontal Rule-->
<div class="iw-base-horizontal-divider"><img src="/iw-cc/base/images/clear.gif" alt="Divider" height="3" width="1"></div>

<!-- Task attributes: workarea, owner, initiated, priority, due date -->
<table width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td nowrap class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.preview'))"/>
    </td>
    <td>
<!-- Workarea link: Show <branch>: <workarea> and use the vpath for hoover text -->
      <a href="/iw-cc/viewarea?vpath=<iw_url_escape><iw_value name="$areapath_utf8"/></iw_url_escape>"
         title="<iw_url_escape><iw_value name="user_task.areavpath@v"/></iw_url_escape>"
         class="iw-base-link"><iw_value name="$project_name"/>: <iw_value name="$workarea_name"/></a>
    </td>
    <td nowrap class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.owner'))"/>
    </td>
    <td>
<iw_comment>
  Include the "Take" action if the subject is an unassigned group task
</iw_comment>
<iw_if expr='{iw_value name="user_task@owner"/} eq "&lt;no user&gt;"'>
<iw_then>
      <a href="/iw-cc/taketask?taskid=<iw_value name="user_task@id"/>"
         class="iw-base-link"><iw_value name="($localizer->format('unassigned'))"/> - <iw_value name="($localizer->format('take.label'))"/></a>
</iw_then>
<iw_else>
      <span class="iw-base-text-field-data">
      <iw_value name="html_escape(iwpt_dcr_value('user_task@owner'))"/>
      </span>
</iw_else>
</iw_if>
    </td>
    <td nowrap class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.activationdate'))"/>
    </td>
    <td class="iw-base-text-field-data">
      <iw_value name="$task_activation"/>
    </td>
  </tr>
  <tr>
    <td nowrap class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.priority'))"/>
    </td>
    <td class="iw-base-text-field-data">
      <iw_value name="$priority"/>
    </td>
    <td nowrap class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.duedate'))"/>
    </td>
    <td class="iw-base-text-field-data">
      <iw_value name="$due_date"/>
    </td>
  </tr>
</table>

<!--Spacer-->
<img src="/base/images/clear.gif" width="10" height="7">

<!-- SUBHEADING: Description and Comments -->
<iw_include pt="ccmail_subheading.tpl"
            title_param_val="($localizer->format('taskdetails.descriptioncomments'))"
            mode="ptlocal" />

<table width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
<iw_comment>
  Include the job description, the job creator, and the job activation time.
</iw_comment>
      <span class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.description'))"/>
      </span>
      <span class="iw-base-text-field-data">
      <iw_value name="html_escape(iwpt_dcr_value('dcr.workflowinformation.workflow.description'))"/>
      [<iw_value name="dcr.workflowinformation.workflow@creator"/>:
       <iw_value name="$job_activation"/>]
      </span>
    </td>
  </tr>
  <tr>
    <td><img src="<%=baseImagePath%>/clear.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td>
<iw_comment>
  Include the last comment (if there is one), and a link to the other comments
  (if there is more than one).  Show "--" if there are no comments.
  XYZZY: Format date like 8/24/2001 2:58 PM EST
</iw_comment>
      <span class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.lastcomment'))"/>
      </span>
      <span class="iw-base-text-field-data">
<iw_if expr='defined($last_comment)'>
<iw_then>
      <iw_value name="html_escape($last_comment)"/>
      [<iw_value name="last_comment@user"/>:
       <iw_value name="POSIX::strftime($localizer->format("datetimeformat"),
                           localtime(iwpt_dcr_value('last_comment@date')))"/>]
<iw_if expr='scalar(@comments) > 1'>
<iw_then>
<iw_comment>
  XYZZY: Add a parameter to "show comments" on task details page
</iw_comment>
      [<a href="/iw-cc/viewtaskdetails?taskid=<iw_value name="user_task@id"/>&show_comments=true"
          class="iw-base-link"><iw_value name="($localizer->format('n_previous', scalar(@comments)))"/></a>]
</iw_then>
</iw_if>
</iw_then>
<iw_else>
      --
</iw_else>
</iw_if>
      </span>
    </td>
  </tr>
  <tr>
    <td><img src="<%=baseImagePath%>/clear.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td>
      <span class="iw-base-text-field-label">
      <iw_value name="($localizer->format('taskdetails.currenttask'))"/>
      </span>
      <span class="iw-base-text-field-data">
      <iw_value name="html_escape(iwpt_dcr_value('user_task.description'))"/>
      </span>
    </td>
  </tr>
</table>

<!--Spacer-->
<img src="/base/images/clear.gif" width="10" height="7">

<!-- SUBHEADING: Attached Files -->
<iw_comment> Include action links if this is an edit task </iw_comment>
<iw_include pt="ccmail_subheading.tpl"
            title_param_val="($localizer->format('taskdetails.attachedfiles'))"
            actions_param_val="(iwpt_dcr_value('user_task@readonly') eq 't' ? undef : filelist_actions_html())"
            mode="ptlocal" />

<iw_comment>
If there are no attached files, display a "No attached files" message.
</iw_comment>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<iw_if expr='scalar(iwpt_dcr_list("email_task.files.file")) > 0'>
<iw_then>
<iw_iterate var='iter' list='email_task.files.file'>
<iw_perl><![CDATA[
  my $file = iwpt_dcr_value('iter@path');
  $file =~ s|\\|/|g;
  my $deleted = iwpt_dcr_value('iter@deleted') eq "t";
  # vpath to file
  my $file_vpath = "$areapath/$file";

  # Convert the file_vpath to utf-8 octets
  my $file_vpath_utf8 = $file_vpath;
  if ( $] ge "5.008") { $file_vpath_utf8 = Encode::encode_utf8($file_vpath) }

  # Full path to the submitted file
  my $file_in_workarea_utf8 = "$iwmnt$file_vpath";
  # Full path to the same file in STAGING.
  my $file_in_staging_utf8 = "$iwmnt$staging/$file";

  my $file_in_workarea_local = $file_in_workarea_utf8;
  my $file_in_staging_local  = $file_in_staging_utf8;
  if ($iwserver_locale ne '') {
      my $enc = TeamSite::I18N_utils::iw_locale_2_encoding( $iwserver_locale );
      $enc = TeamSite::I18N_utils::promote_encoding( $enc );
      $file_in_workarea_local = 
         TeamSite::I18N_utils::utf8_to_other_encoding($enc, 
                                                      $file_in_workarea_utf8);
      $file_in_staging_local = 
         TeamSite::I18N_utils::utf8_to_other_encoding($enc, 
                                                      $file_in_staging_utf8);
  }
  my $isNew = ! -e $file_in_staging_local;
  my $isFolder = -d $file_in_workarea_local;

  # Determine the icon to use for this file
  # This will be one of the following: hole, directory, file
  # XYZZY: Ideally, we would use different icons for locked, modified, etc.
  # See com/interwoven/teamsite/filesys/context/CSNodeContext
  my $file_icon = "/iw-cc/teamsite/images/file_images/";
  if ($deleted) {
    $file_icon .= "img_hole_file.gif";
  } elsif ($isFolder) {
    $file_icon .= "img_directory.gif";
  } elsif ($isNew) {
    $file_icon .= "img_file.gif";
  } else {
    $file_icon .= "img_file_modified.gif";
    # Other possibilities:
    # img_file_locked_modified.gif
  }
  $row_style = ($row_odd ? "iw-base-listview-row-odd" : "iw-base-listview-row-even");
  $row_odd = ! $row_odd;

]]></iw_perl>
  <tr height="16" class="<iw_value name="$row_style"/>">
    <td width="100%">
      <img alt="[icon]" src="<iw_value name="$file_icon" />" class="widget-link-icon" />
<iw_if expr='$isFolder'>
<iw_then>
      <span class="iw-base-text-field-label"><iw_value name="iter@path"/></span>
</iw_then>
<iw_else>
      <a href="/iw-cc/previewfile?vpath=<iw_url_escape><iw_value name='$file_vpath_utf8'/></iw_url_escape>"
         class="iw-base-link"><iw_value name="iter@path"/></a>
</iw_else>
</iw_if>
    </td>
    <td align="left">
<iw_value name="file_actions_html($file_vpath, $isNew, $deleted, $isFolder, scalar(iwpt_dcr_list('iter.comments.comment')))"/>
    </td>
  </tr>
</iw_iterate>
</iw_then>
<iw_else>
  <tr>
    <td colspan="2">
      <center><div class="iw-base-text-no-content"><iw_value name="($localizer->format('taskdetails.nofiles'))"/></div></center>
    </td>
  </tr>
</iw_else>
</iw_if>
</table>

<!-- SUBHEADING: Description and Comments -->
<iw_include pt="ccmail_subheading.tpl"
            title=" "
            mode="ptlocal" />

<!-- Task Transition -->
<table width="100%" border="0" cellpadding="3" cellspacing="0">
    <tr>
    <td nowrap align="right">
<iw_include pt="ccmail_button.tpl"
            label_param_val="($localizer->format('finish_button.label'))"
            mode="ptlocal">
<![CDATA[
    $iw_param{href} = "/iw-cc/transitiontask?taskid="
                      . iwpt_dcr_value('user_task@id');
]]>
</iw_include>
    </td>
  </tr>
</table>

</body>
</html>


