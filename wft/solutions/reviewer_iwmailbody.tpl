<iw_comment>
File: /iw-home/local/config/wft/solutions/reviewer_iwmailbody.tpl

This is an HTML Mail Body presentation template used to notify a reviewer
of a pending task.  It can be used with iw_solution_email.ipl to create
the HTML body of an email message.

We assume that:
  dcr.workflowinformation.task[0] is the email task itself, and
  dcr.workflowinformation.task[1] is the target (Review) task.

</iw_comment>
<iw_include pt="ccmail_htmlbody.tpl"
            type_of_task_param="review"
            mode="ptlocal" />
