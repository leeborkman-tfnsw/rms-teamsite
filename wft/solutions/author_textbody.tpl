<iw_comment>
File: /iw-home/local/config/wft/solutions/author_iwmailbody.tpl

This is a Text Mail Body presentation template used to notify an author
of pending work.  It can be used with iw_solution_email.ipl to create
the text body of an email message.

We assume that:
  dcr.workflowinformation.task[0] is the email task itself, and
  dcr.workflowinformation.task[1] is the target (Author Work) task.

</iw_comment>
<iw_include pt="ccmail_textbody.tpl"
            type_of_task_param="author"
            mode="ptlocal" />
