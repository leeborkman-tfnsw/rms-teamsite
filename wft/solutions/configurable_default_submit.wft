<template_script><![CDATA[
#------------------------------------------------------------------------------
#  File      configurable_default_submit.wft 
#  Use       Configurable default submit workflow
#
#  This workflow is provided as an alternative to the default default_submit
#  workflow.  It is intended to be used when submitting modified files
#  in a role other than Author.
#
#  A job created with this workflow template will have -- at a minimum --
#  the following tasks:
#    Submit
#    End
#
#  In addition, it may have any of the following OPTIONAL tasks:
#    Metadata Capture
#    Deploy
#    Notification of Deployment Problem
#    Resolve Deployment Problem
#
#  This template uses the following HTTP request keys:
#
#    iw_user           The name (user id) of the current user (user id)
#    iw_workarea       The workarea's vpath
#    iw_areaowner      The owner of the workarea (user id)
#    iw_file           The list of files to be worked on, each relative
#                       to iw_workarea
#    iw_template_file  The path to this template file relative to
#                       iw-home/local/config/wft
#    iw_locale         The locale of the current user
#
#  Copyright 1999, 2002 Interwoven Inc.
#  All rights reserved.
#------------------------------------------------------------------------------

use strict; # Try to catch errors as early as possible.

# Declare which external packages and library routines will be used.
use TeamSite::Config;
use TeamSite::JavaResourceBundle;
use TeamSite::WFconstructor;
use TeamSite::IW_cfg;

my $iwcfg = TeamSite::IW_cfg->new();
$iwcfg->read();
my $web_host = $iwcfg->lookup('iwwebd', 'host', 'localhost');
my $web_port = $iwcfg->lookup('iwwebd', 'http_port','80');
if($web_port ne "80") {
	$web_host = $web_host . ":" . $web_port; 
}

# Set $debugging to TRUE to enable debugging.
my $debugging = FALSE;
my $temp_dir = (($^O eq "MSWin32") ? "C:/tmp/" : "/tmp/");

my $iw_user = __VALUE__('iw_user');
my $iw_workarea = __VALUE__('iw_workarea');
my $iw_areaowner = __VALUE__('iw_areaowner');
my $iw_template_file = __VALUE__('iw_template_file');

######################################################################
# Load the appropriate resource bundle for localization of this page.
######################################################################

# The properties file should be in the same directory as this
# workflow template, and should have the same name but end in
# _<locale>.properties (ex. _ja.properties) or .properties instead
# of .wft.

my $iwhome = TeamSite::Config::iwgethome();
$_ = $iw_template_file;
m|(.*)/([^/]+).wft$|i;
my $wft_base_name = $2;
my $this_wft_dir = TeamSite::Config::iwgetlocation("iwconfigs") . "/wft/$1";
my $default_debug_file = $temp_dir . $wft_base_name . ".xml";

my $propfile = $wft_base_name . "_" . __VALUE__('iw_locale');
my $localizer = TeamSite::JavaResourceBundle->new();
my @bundle = $localizer->load( $this_wft_dir, $propfile ); 

my $job_name = $localizer->format('job.name');
my $job_description = $localizer->format('job.description');
my $template_name = $localizer->format('template_name');
my $done_label = $localizer->format('done_label');
my $retry_label = $localizer->format('retry_label');
my $cancel_job_label = $localizer->format('cancel_job_label');

######################################################################
# Load values from the configuration file for this workflow.
######################################################################

# The configuration file should be in the same directory as this
# workflow template, and should have the same name but end in .cfg
# instead of .wft.
my $wft_config = TeamSite::JavaResourceBundle->new();
$wft_config->load( $this_wft_dir, $wft_base_name, "cfg", 0 )
    or die "Configuration file not found.";

my $metadata_capture_ui = $wft_config->get_value('metadata_capture_ui');
my $ask_for_info = $wft_config->get_value('ask_for_info') eq 'yes';
my $include_copy_comments_button = 
    $wft_config->get_value('include_copy_comments_button') eq 'yes';

######################################################################
# Task properties
######################################################################

######################################################################
# Build the UI controls for user input.
######################################################################

my $pre_html = '';
if ($include_copy_comments_button) {
    $pre_html =<<'EOS';
<SCRIPT LANGUAGE="Javascript">
function makeArray( elements )
{
    var propNames = new Array();
    for (var i = 0; i < elements.length; i++)
    {
        var elem = (typeof elements.item == 'undefined')
                   ? elements[i]
                   : elements.item(i);
        if (elem.name.indexOf('iw_file_comment_') != -1)
        {
            propNames[propNames.length] = elem.name;
        }
    }
    return propNames;
}

function copy_file_comment(win)
{
    var first = null;
    var nameList = makeArray ( win.document.forms[0].elements);
    for (var i = 0; i < nameList.length; i++)
    {
      field_name = nameList[i];
      if ((first == null) && (field_name == 'iw_file_comment_0'))
      {
          first = win.document.forms[0].elements[field_name].value;
      }
      else
      {
          if (first != '')
          {
              win.document.forms[0].elements[field_name].value = first + '';
          }
          else
          {
              win.document.forms[0].elements[field_name].value = '';
          }
      }
    }
}
</SCRIPT>
EOS
}

my $banner_html=<<EOS;
<table cellspacing='3' cellpadding='3' class="iw-base-heading" style="width:100%;margin-bottom:4px;">
    <tr valign='top' width='100%'>
        <td class="iw-base-heading-title">$template_name</td>
    </tr>
</table>
EOS

CGI_info(
    title               => $localizer->format('page_title'),
    error_label_bgcolor => "#EEEEEE" ,
    locale              => __VALUE__('iw_locale'),
    submit_label        => $localizer->format('submit_button_label'),
    cancel_label        => $localizer->format('cancel_button_label'),
    banner_html         => $banner_html,
    pre_tagtable_html   => $pre_html,
);

# Force the form to show up at least once (because there are no
# required fields
# ::TODO:: make submit comments requirement be configurable
if (! defined( __VALUE__('iw_first_time') ) ) {
    TAG_info(
        iw_first_time_hidden_placeholder =>
            [ html        => "<input type='hidden' value=''>",
              is_required => 'true',
            ],
    );
}

TAG_info(
    iw_submit_comments_header =>
        [ html        => "<span></span>",
          is_required => 'false',
          label       => "</td><td></td></tr>"
                         . "<tr><td class='iw-base-heading-title' "
                         . "style='background-color: #EEE; padding: 4px;' "
                         . "colspan=2>"
                         . $localizer->format('submit_comments_header_label')
                         . "</td></tr>"
                         . "<tr><td>",
        ],
    # The value of the tag "iw_submit_comment" (obtained from the user)
    # will appear as a user variable on the submit task.
    iw_submit_comment  =>  
        [ html        => "<textarea class='iw-base-text-field-data' rows='4' style='width:100%'></textarea>",
          error_msg   => $localizer->format('submit_comment_error_msg'),
          is_required => 'false',
          label       => $localizer->format('submit_comment_label'),
        ],
);

# Add the iw_info_field to the form if it has been activated.
if ($ask_for_info) {
    TAG_info(
        iw_info_field  =>  
            [ html        => "<textarea class='iw-base-text-field-data' rows='2' style='width:100%'>"
                             . "</textarea>",
              error_msg   => $localizer->format('info_label_error_msg'),
              is_required => 'false',
              label       => $localizer->format('info_label1'),
            ],
    );
}

# This is a list of options for this workflow.  Each option has:
#   'var'   - the name of the variable that will hold its value.
#   'ask'   - a Boolean that indicates whether the user should be prompted
#             for this option's value in the job creation form.
#   'value' - the value for this variable.  If 'ask' is true, this
#             will be used as the default value.
#   'label' - the label that will identify this option if it appears in
#             the job creation form.
my @wft_options = (
    { var   => "iw_override",
      value => $wft_config->get_value('override') eq "yes",
      ask   => $wft_config->get_value('ask_override') eq "yes",
      label => $localizer->format('override_label'),
    },
    { var   => "iw_keep_locks",
      value => $wft_config->get_value('keep_locks') eq "yes",
      ask   => $wft_config->get_value('ask_keep_locks') eq "yes",
      label => $localizer->format('keep_locks_label'),
    },
    { var   => "include_metadata_capture",
      value => $wft_config->get_value('metadata_capture') eq "yes",
      ask   => $wft_config->get_value('ask_metadata_capture') eq "yes",
      label => $localizer->format('metadata_capture_label'),
    },
    { var   => "include_deploy_task",
      value => $wft_config->get_value('deploy_task') eq "yes",
      ask   => $wft_config->get_value('ask_deploy_task') eq "yes",
      label => $localizer->format('include_deploy_task_label'),
    },
    { var   => "include_no_deploy_email_task",
      value => $wft_config->get_value('email_no_deploy') eq "yes",
      ask   => $wft_config->get_value('ask_email_no_deploy') eq "yes",
      label => $localizer->format('no_deploy_email_label'),
    },
  );

# Add the workflow options to the job creation form.
for my $wft_option (@wft_options) {
  # Set the value of the variable.
  my $var = $wft_option->{var};

  # Can't use string as a SCALAR ref while "strict refs" in use.
  no strict 'refs';
  $$var = $wft_option->{value};

  if ( $wft_option->{ask} ) {
    # If "ask" has been requested for a wft option, then include radio boxes
    # in the job creation form so the initiator can select or confirm the
    # value.  Use the "value" as the default.
    TAG_info(
        $var =>
           [ html        => "<span style='font-size:11px;font-weight:normal'>"
                            . "&nbsp;"
                            . "<input type='radio' "
                            . ($$var ? "checked " : "")
                            . "value='true'>"
                            . "&nbsp;"
                            . $localizer->format('yes_label')
                            . "&nbsp;"
                            . "<input type='radio' "
                            . ($$var ? "" : "checked ")
                            . "value='false'>"
                            . "&nbsp;"
                            . $localizer->format('no_label'),
             is_required => 'true',
             label       => "<span style='font-size:11px;'>"
                            . $wft_option->{label}
                            . "</span>",
           ]
    );
  } else {
    # If "ask" has not been requested, include the value as a hidden field.
    TAG_info(
        $var =>
           [ html        => "<input type='hidden' value='" 
                            . ($$var ? TRUE : FALSE)
                            . "'>\n",
             is_required => 'false',
           ]
    );
  }
}

# Add debugging fields to the form if debugging has been switched on.
if ($debugging) { 
    TAG_info(
       iw_debug_mode  =>
           [ html        => "<input type='checkbox' value='true' checked>",
             is_required => 'false',
           ],
       iw_output_file =>
           [ html        => "<input type='text' value='" . $default_debug_file . "' size='40'>",
             is_required => 'false',
           ],
    );
}

# Take the list of files in 'iw_file' and sorted them in unencoded form.
my @submit_file = (__ELEM__('iw_file') ? sort __VALUE__('iw_file') : ());

# For each file, add a textarea in the job creation form for file comments.
# A heading ("Individual File Comments") preceeds the first file.
TAG_info(
    iw_file_comments_header =>
        [ html        => '<span></span>',
          is_required => 'false',
          label       => "</td><td></td></tr></table><table style='width:100%;' cellpadding='0' cellspacing='0' border='0'>"
                         . "<tr><td class='iw-base-heading-title' "
                         . "style='background-color: #EEE; padding: 4px;' "
                         . "colspan=2>"
                         . $localizer->format('info_label2')
                         . "</td></tr>"
                         . "<tr><td width=70%></td><td></td></tr><tr><td>",
        ],
);
my $copy_comments_label = $localizer->format('copy_comments_label');
my $copy_comments_html = '';
if ($include_copy_comments_button) {
    $copy_comments_html =<<EOS;
<div align="right" style='padding-top:8px;'>
<table cellspacing="0" cellpadding="0" border="0">
  <tr>
  <td width="1"><img src="/iw-cc/base/images/dialog_btn_left.gif"/></td>
  <td nowrap valign="middle" align="middle" 
      background="/iw-cc/base/images/dialog_btn_mid.gif">
    <a href="javascript:copy_file_comment(self)" 
       class="iw-base-actionlist-link">

$copy_comments_label

    </a>
  </td>
  <td width="1"><img src="/iw-cc/base/images/dialog_btn_right.gif"/></td>
  </tr>
</table>
</div>
EOS
}
for (my $i=0; $i<@submit_file; ++$i) {
    TAG_info(
        "iw_file_comment_$i" => 
            [ html        => "<span></span>",
              error_msg   => $localizer->format('comment_label_error_msg'),
              is_required => 'false',
              label       => "</td><td></td></tr>"
                             . "<tr><td class='iw-base-text-field-label' "
                             . "style='padding-left:4px;' colspan='2'>"
                             . TeamSite::CGI_lite::escape_html_data($submit_file[$i])
                             . "<div>"
                             . "<textarea class='iw-base-text-field-data' "
                             . "name='iw_file_comment_$i' "
                             . "style='width:100%;' rows='3'>"
                             . "</textarea></div>"
                             . ( (($i == 0) && ($#submit_file > 0))
                                 ? $copy_comments_html : '' )
                             . "</td></tr><tr><td>",
            ],
    );
}

######################################################################
# SUBROUTINES
######################################################################

######################################################################
# WORKFLOW BODY
######################################################################

{
    # A network approach to handling tasks linkages; a seven step process:
    # Step 1: Declare the basic properties of the workflow.
    # Step 2: Declare all the tasks.
    # Step 3: Declare links between tasks.
    # Step 4: Identify the 'dormant' tasks and propagate.
    # Step 5: Compute activation sets.
    # Step 6: Any "special" task handling.
    # Step 7: Generate the job spec.

    # Step 1: Declare the basic properties of the workflow.

    my @file_comments = ();
    for (my $i=0; $i < @submit_file; ++$i) {
        push @file_comments, __VALUE__("iw_file_comment_$i");
    }
    declare_workflow(name        => $job_name,
                     owner       => $iw_user,
                     creator     => $iw_user,
                     description => $job_description,
                     comment     => "This job was created using $iw_template_file",
                     files       => \@submit_file,
                     file_comments => \@file_comments);

    # Step 2: Declare all the tasks.

    declare_task('resolve_deploy',
                 name        => $localizer->format('resolve_deploy_task.name'),
                 description => $localizer->format('resolve_deploy_task.description'),
                 type        => "usertask",
                 owner       => $iw_areaowner,
                 areavpath   => $iw_workarea);
    declare_task('submit',
                 name        => $localizer->format('submit_task.name'),
                 description => $localizer->format('submit_task.description'),
                 type        => "submittask",
                 owner       => $iw_user,
                 areavpath   => $iw_workarea,
                 unlock      => (__VALUE__('iw_keep_locks') eq 'true' 
                                ? FALSE : TRUE),
                 override    => (__VALUE__('iw_override') eq 'true' 
                                ? TRUE : FALSE),
                 savecomments => TRUE);
    # Add variables to the Submit task.
    set_task_variable('submit', 'submit_cmt', "__TAG__('iw_submit_comment');");
    {
        # If iw_info_field has anything other than whitespace, we want to 
        # include its value as the submittask variable 'submit_info'.
        my $iw_info_field = __VALUE__('iw_info_field');
        if ($iw_info_field =~ /\S+/) {
            set_task_variable('submit', 'submit_info', $iw_info_field);
        }
    }
    declare_task('metadata',
                 name        => $localizer->format('metadata_task.name'),
                 description => $localizer->format('metadata_task.description'),
                 type        => "cgitask",
                 start       => TRUE,
                 owner       => $iw_user,
                 areavpath   => $iw_workarea,
                 immediate   => TRUE,
                 command     => $metadata_capture_ui);
    declare_task('conflictchk',
                 name        => $localizer->format('conflict_check_task.name'),
                 description => $localizer->format('conflict_check_task.description'),
                 type        => "cgitask",
                 start       => FALSE,
                 owner       => $iw_user,
                 areavpath   => $iw_workarea,
                 timeout     => "+000001", # 1 minute
                 immediate   => TRUE,
                 command     => "iwconflictcheck.cgi");
    set_task_variable('conflictchk', 'submittask_name', $localizer->format('submit_task.name'));
    declare_task('no_deploy_email',
                 name        => $localizer->format('no_deploy_email_task.name'),
                 description => $localizer->format('no_deploy_email_task.description'),
                 type        => "externaltask",
                 owner       => $iw_user,
                 areavpath   => $iw_workarea,
                 timeout     => "+000001", # 1 minute
                 command     => "http://$web_host/iw-cc/urlexternaltask",
                 variables   => {
                     "ClassName" => "com.interwoven.ui.teamsite.workflow.task.urltask.DeployFailedNotificationTask",
                     "mail_template" => $wft_config->get_value('no_deploy_email_template'),
                     "resolve_deploy_task" => $localizer->format('resolve_deploy_task.name'),
                     "deploy_task" => $localizer->format('deploy_task.name')
                     });
    declare_task('deploy',
                 name        => $localizer->format('deploy_task.name'),
                 description => $localizer->format('deploy_task.description'),
                 type        => "externaltask",
                 owner       => $iw_areaowner,
                 areavpath   => $iw_workarea,
                 command     => "http://$web_host/iw-cc/urlexternaltask",
                 variables   => {
                     "ClassName" => "com.interwoven.ui.teamsite.workflow.task.urltask.DeployURLExternalTask",
                     "odDeploymentName" => $wft_config->get_value('deployment_name')
                     });
    declare_task('end',
                 name        => $localizer->format('end_task.name'),
                 type        => "endtask");

    # Step 3: Declare links between tasks.
    # NB: When a task has more than one link to successor tasks, the order
    # in which the links are declared is significant.  It controls the 
    # order of the successorsets, which is very important to an externaltask.
    link_tasks('metadata', 'conflictchk', $done_label);
    link_tasks('conflictchk', 'submit', $done_label);
    link_tasks('submit', 'deploy', $done_label);
    link_tasks('deploy', 'end', "Success");
    link_tasks('deploy', 'no_deploy_email', "Failure");
    link_tasks('no_deploy_email', 'resolve_deploy', $done_label);
    link_tasks('resolve_deploy', 'deploy', $retry_label);
    link_tasks('resolve_deploy', 'end', $cancel_job_label);

    # Step 4: Identify the 'dormant' tasks and propagate.
    if((!__VALUE__('include_metadata_capture')) ||
        (__VALUE__('include_metadata_capture') eq 'false')) {
        declare_dormant('metadata');
    }

    if ((! __VALUE__('include_deploy_task')) ||
        (__VALUE__('include_deploy_task') eq 'false')) {
        declare_dormant('deploy', 'no_deploy_email', 'resolve_deploy');
    } elsif ((! __VALUE__('include_no_deploy_email_task')) ||
        (__VALUE__('include_no_deploy_email_task') eq 'false')) {
        declare_dormant('no_deploy_email');
    }

    propagate_from_dormant_tasks();

    # Step 5: Compute activation sets.
    compute_activation_sets();

    # Step 6: Any "special" task handling.

    # Step 7: Generate the job spec.
    __INSERT__(get_job_spec());
}

]]></template_script>
