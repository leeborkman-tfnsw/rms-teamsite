How to enable VisualAnnotate in workflow
----------------------------------------

If you have installed VisualAnnotate on your TeamSite server you 
may enable VisualAnnotate in the Configurable Author Submit workflow 
(configurable_author_submit.wft) or Configurable Author
Assignment workflow (configurable_author_assignment.wft).  This 
means that VisualAnnotate-specific emails will be sent, and 
reviewers will be able to annotate task files in their browsers 
with the VisualAnnotate toolbar.

WHAT YOU NEED TO DO:

The following apply to the configuration files for the solutions 
workflows (configurable_author_submit.cfg or 
configurable_author_assignment.cfg).  Sections with asterisks (*) are 
required.

* Enable VA
-----------
Set the "va_enabled" property to "yes".  Also make sure that the
"va_enabled" property in the [visualannotate] section of 
iw.cfg is set to "true" (the VisualAnnotate installer does this last 
part automatically).

Enable Email Notification
-------------------------
Email notification is disabled by default in the solutions workflows. 
Set the "email_notification_to_author" and 
"email_notification_to_reviewer" properties to "yes" to enable email.

If you need to use an email mapping file, you must edit the email map 
that's located in the solutions directory (email_map.cfg).

Configure Review Properties
---------------------------
Scroll to the "Review" section of the configuration file. 
(This section begins with "Who should review the Author's work?").  
You must edit this section of the configuation file if you want the 
job initiator to be able to select reviewer(s) from a dropdown list of 
possible reviewers.  The default reviewer is the Workarea owner.  See 
configurable_author_submit.cfg for documented details.  

The following are some sample values for Review section properties.

   1. This configuration describes a serial review process in which 
      the job initiator can select up to 2 reviewers from a list of 
      all users in admin, master, and editor roles. 
     
      review_type=serial[select[role:admin,role:master,role:editor],select[role:admin,role:master,role:editor]]
      return_on_first_reject=no
      min_reviewers=1  

   2. This configuration describes a serial review process in which 
      the job initiator must select 2 reviewers.  The first reviewer 
      is a TeamSite Editor and the second is a TeamSite Master. 
      If the first reviewer rejects the content, the second reviewer 
      is bypassed and the workflow returns to the author for revisions.

      review_type=serial[select[role:editor],select[role:master]]
      return_on_first_reject=yes

Note that VisualAnnotate does not support concurrent review.  If you 
define a concurrent review process for use with VisualAnnotate you 
will get an error message.  You may, however, choose to simulate 
concurrent review by defining a serial review process and setting 
"return_on_first_reject" to "no".  This ensures that the workflow 
will not return to the author until all reviewers have reviewed the 
content.

FREQUENTLY ASKED QUESTIONS:

1.  How do I know whether VisualAnnotate is installed?

If VisualAnnotate is installed on your TeamSite server, there 
will be a [visualannotate] section in iw.cfg with a property 
called "va_enabled".  The VisualAnnotate installer automatically 
sets this property to "true".

In addition, the installer will create a directory called 
"vannotate" under iw-home/local/config/wft/.  This is where 
the VisualAnnotate email templates are located. 

2.  I can't find "va_enabled" in the configurable_author_*.cfg file! 
What gives?

If this file existed before you installed TeamSite 5.5.2 SP2, the 
Service Pack installer will NOT overwrite it.  Instead, the newest
version of the file will be installed as 
configurable_author_*.cfg.example.  The .example files contain the 
VisualAnnotate properties.  You must either rename the .example files 
or copy the VisualAnnotate logic into your existing .cfg files.
