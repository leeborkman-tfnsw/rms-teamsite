<iw_comment>
File: <iw-home>/local/config/wft/solutions/ccmail_header.tpl
        
This is a Mail Header presentation template for use with Content Center.
It can be used with iw_solution_email.ipl to create an email header.

This template is applied to an XML document that contains information
regarding a workflow job and tasks within that job.  The output of the
template is an XML file that conforms to the DTD header_xml.dtd in the
<iw-home>/local/config/wft/solutions directory.

If you want to check the well-formedness of the XML file, you can use 
the tool: iw-home/private/bin/iwxmlval <path-to-the-xml-file>

We assume that:
  dcr.workflowinformation.task[0] is the email task itself, and
  dcr.workflowinformation.task[1] is the subject task.

</iw_comment>

<iw_perl>
<![CDATA[

use Date::Manip qw(UnixDate);
use TeamSite::PTparser qw(xml_escape);

# Use GMT as the timezone if it cannot be determined.
if (($^O eq "MSWin32") && ! exists $ENV{TZ} && ! defined $main::TZ)
{ $main::TZ = "GMT"; }

# TeamSite installation directory
(my $iwhome = TeamSite::Config::iwgethome()) =~ tr|\\|/|;

my $user_task = iwpt_dcr_list('dcr.workflowinformation.task')[1];
my $areavpath = iwpt_dcr_value('user_task.areavpath@v');
my $workarea_name = "$areavpath";
$workarea_name =~ s/^.+WORKAREA[\/\\](.+)$/$1/;
# For the project name, use the last segment of the branch name.
my $project_name = "$areavpath";
if ($project_name =~ m/([^\/\\]+)[\/\\](WORKAREA|STAGING)/) {
    $project_name = $1;
}

# Get the host name
my ($iwcfg) = "$iwhome/bin/iwconfig iwwebd";
(my $hostname = `$iwcfg host`) =~ s/\n$//;

my $message_id = '<IWOV.' . (int(rand 32000) + 1) . '@' . $hostname . '>';

# For the Subject: line, use the following from the user task:
#  - the project name
#  - the task name
#  - the task ID (in square brackets)
#  - the task description, and
#  - if this is a group task without an owner, "(Group Task)"
my $subject = "$project_name "
            . iwpt_dcr_value('user_task@name')
            . " [" . iwpt_dcr_value('user_task@id') . "]: "
            . iwpt_dcr_value('dcr.workflowinformation.workflow.description');
if (iwpt_dcr_value('user_task@owner') eq '<no user>') {
    $subject .= ' (Group Task)';
}

sub xml_escape
{
    return TeamSite::PTparser::xml_escape(shift);
}

]]>
</iw_perl>

<headers>
<iw_comment>

Send the message to the owner of the task.
If the target task does not have an owner (i.e., owner = "<no user">)
then send the task to each user and group in the "users" element.

You can also hard-wire addresses.  For example:
  <iwov_emailmap user='jsmith' />

</iw_comment>
    <to>
        <iw_if expr='{iw_value name="user_task@owner"/} ne "&lt;no user&gt;"'>
        <iw_then>
            <iwov_emailmap user='{iw_value name="user_task@owner"/}' />
        </iw_then>
        <iw_else>
            <iw_iterate var='user' list='user_task.users.user'>
                <iwov_emailmap user='{iw_value name="user@v"/}' />
            </iw_iterate>
            <iw_iterate var='group' list='user_task.users.group'>
                <iwov_emailmap user='{iw_value name="group@v"/}' />
            </iw_iterate>
        </iw_else>
        </iw_if>
    </to>
    <!-- You can CC additional addresses.  For example: -->
    <cc>
        <!-- <iwov_emailmap user='jane.doe' /> -->
    </cc>
    <!-- From the owner of the email task -->
    <from>
        <iwov_emailmap user='{iw_value name="dcr.workflowinformation.task[0]@owner"/}' />
    </from>
    <subject>
      <iw_value name="xml_escape($subject)"/>
    </subject>

    <!-- Other headers -->
    <other_header name="Date"><iw_value name='UnixDate("today","%g")'/></other_header>
    <other_header name="Message-ID"><iw_value name="xml_escape($message_id)"/></other_header>
</headers>

