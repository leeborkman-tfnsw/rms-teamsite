<!-- 
File: /iw-home/local/config/wft/solutions/no_deploy_iwmailbody.tpl
        
This is an HTML Mail Body presentation template used to report a
problem that requires user intervention.

It is used to create the HTML body of an email message.  It takes
workflow and task information as XML input.

We assume that:
  dcr.workflowinformation.task[0] is the email task itself
  dcr.workflowinformation.task[1] is the "resolve conflict" task, and
  dcr.workflowinformation.task[2] is the task that had the problem.

-->

<iw_perl>
<![CDATA[
use TeamSite::Config;
# TeamSite installation directory
(my $iwhome = TeamSite::Config::iwgethome()) =~ tr|\\|/|;

my $url = get_base_url();

# url for getting images under iw-icons/solutions
my $url_image = $url . "/iw-icons/solutions";

# url for webdesk
my $url_web = $url . "/iw/webdesk";

my $areapath = iwpt_dcr_value('dcr.workflowinformation.task[0].areavpath@v');
$areapath =~ s|\\|/|g;

sub get_base_url
{
    # get the web daemon information
    my ($iwcfg) = "$iwhome/bin/iwconfig iwwebd";

    # using HTTP or HTTPS
    (my $proto = `$iwcfg default_protocol`) =~ s/\n$//;

    # get the host name
    (my $hostname = `$iwcfg host`) =~ s/\n$//;

    # get the port number
    (my $port = `$iwcfg ${proto}_port`) =~ s/\n$//;
    my($url) = "$proto://" . $hostname;

    # don't bother showing port if it is the default port
    if (($proto eq "http" && ($port != 80 && $port ne ""))
        || ($proto eq "https" && ($port != 443 && $port ne ""))) {
        $url .= ":$port";
    }

    return $url;
}

]]>
</iw_perl>


<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<style><!--
.iwhead {
    font-family: verdana;
    font-size: 12px;
    background-color: #eeeeee;
}
.iwbody {
    font-family: verdana;
    font-size: 12px;
}
--></style>

<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0">
	<!-- Logo -->
	<tr>
		<td colspan="4"><img src="<iw_value name="$url_image"/>/ts_logo.gif" alt="[logo]"></td>
	</tr>

	<!-- Banner -->
	<!-- Divider -->
	<tr><td colspan="4" background="<iw_value name="$url_image"/>/bg_divider.gif"><img src="<iw_value name="$url_image"/>/clear.gif" width="1" height="1" alt="----------"></td></tr>
	<tr>
		<td>&nbsp;</td>
		<td class="iwbody" colspan="3">This is an automated notification from TeamSite.  A problem was encountered while performing a workflow task: <b><iw_value name='dcr.workflowinformation.task[2]@name'/></b>.  Please resolve this problem.</td>
	</tr>

	<!-- Task Description -->
	<!-- Divider -->
	<tr><td colspan="4" background="<iw_value name="$url_image"/>/bg_divider.gif"><img src="<iw_value name="$url_image"/>/clear.gif" width="1" height="1" alt="----------"></td></tr>
	<tr>
		<td class="iwhead"><img src="<iw_value name="$url_image"/>/icn_bullet.gif"></td>
		<td class="iwhead" colspan="3"><b>Task Description</b></td>
	</tr>
	<tr>	
		<td>&nbsp;</td>
		<td class="iwbody" colspan="3"><iw_value name="dcr.workflowinformation.task[1].description"/></td>
	</tr>

	<!-- Job Description -->
	<!-- Divider -->
	<tr><td colspan="4" background="<iw_value name="$url_image"/>/bg_divider.gif"><img src="<iw_value name="$url_image"/>/clear.gif" width="1" height="1" alt="----------"></td></tr>
	<tr>
		<td class="iwhead"><img src="<iw_value name="$url_image"/>/icn_bullet.gif"></td>
		<td class="iwhead" colspan="3"><b>Job Description</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="iwbody" colspan="3"><iw_value name="dcr.workflowinformation.workflow.description"/></td>
	</tr>

	<!-- More Task Details... -->
	<tr>
		<td><img src="<iw_value name="$url_image"/>/icn_option.gif" alt=">>>"></td>
		<td class="iwbody" colspan="3"><a href="<iw_value name="$url_web"/>/task?taskid=<iw_value name="dcr.workflowinformation.task[1]@id"/>" title="Launch WebDesk">More Task Details...</a></td>
	</tr>
	<!-- Finish Task -->
	<tr>
		<td><img src="<iw_value name="$url_image"/>/icn_option.gif" alt=">>>"></td>
		<td class="iwbody" colspan="3"><a href="<iw_value name="$url_web"/>/transitiontask?taskid=<iw_value name="dcr.workflowinformation.task[1]@id"/>" title="Task Transition">Finish this task...</a></td>
	</tr>

	<!-- File List -->
	<!-- Divider -->
	<tr><td colspan="4" background="<iw_value name="$url_image"/>/bg_divider.gif"><img src="<iw_value name="$url_image"/>/clear.gif" width="1" height="1"></td></tr>
	<tr>
		<td class="iwhead"><img src="<iw_value name="$url_image"/>/icn_bullet.gif" alt="*"></td>
		<td class="iwhead" colspan="3"><b>File List</b></td>
	</tr>
	<iw_iterate var='iter' list='dcr.workflowinformation.task[0].files.file'>
	<iw_perl>
	<![CDATA[
	my $file = iwpt_dcr_value('iter@path') ;
	$file =~ s|\\|/|g;
	]]></iw_perl>
	<tr>
		<td><img src="<iw_value name="$url_image"/>/icn_doc.gif"></td>
		<td class="iwbody" colspan="3"><iwov_webdesk_url
		          path='{iw_value name="$areapath"/}/{iw_value name="$file"/}'
		          action='sce'
		          label='{iw_value name="$file"/}'/></td>
	</tr>
	</iw_iterate>
	<!-- Divider -->
	<tr><td colspan="4" background="<iw_value name="$url_image"/>/bg_divider.gif"><img src="<iw_value name="$url_image"/>/clear.gif" width="1" height="1"></td></tr>

	<!-- Job Comments.  Show the most recent comment first. -->
	<tr>
		<td class="iwhead"><img src="<iw_value name="$url_image"/>/icn_bullet.gif" alt="*"></td>
		<td class="iwhead" colspan="3"><b>Job Comments</b></td>
	</tr>
	<iw_iterate var='iter' list="reverse iwpt_dcr_list('dcr.workflowinformation.task[0].comments.comment')">
	<iw_perl>
	<![CDATA[
	my $date = iwpt_dcr_value('iter@date') ;
	my $realdate = localtime($date);
	my $taskid = iwpt_dcr_value('iter@task');
	my $user = iwpt_dcr_value('iter@user');
	my $comments = iwpt_dcr_value('iter');
	]]></iw_perl>

	<tr>
		<td>&nbsp;</td>
		<td class="iwbody">Date: <iw_value name="$realdate"/></td>
		<td class="iwbody">User: <iw_value name="$user"/></td>
		<td class="iwbody">Task: <iw_value name="$taskid"/></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="iwbody" colspan="3">Comments: <iw_value name="$comments"/></td>
	</tr>
	</iw_iterate>
</table>

</body>
</html>
