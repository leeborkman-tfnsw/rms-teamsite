<iw_comment>
File: <iw-home>/local/config/wft/solutions/reviewer_iwmailheader.tpl
        
This is a Mail Header presentation template for review tasks.
It can be used with iw_solution_email.ipl to create an email header.

This template is applied to an XML document that contains information
regarding a workflow job and tasks within that job.  The output of the
template is an XML file that conforms to the DTD header_xml.dtd in the
<iw-home>/local/config/wft/solutions directory.

If you want to check the well-formedness of the XML file, you can use 
the tool: iw-home/private/bin/iwxmlval <path-to-the-xml-file>

We assume that:
  dcr.workflowinformation.task[0] is the email task itself, and
  dcr.workflowinformation.task[1] is the target (Review) task.

</iw_comment>
<iw_include pt="ccmail_header.tpl"
            type_of_task_param="review"
            mode="ptlocal" />
