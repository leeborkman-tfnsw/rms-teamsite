<!-- 
	/iw-home/local/config/wft/solutions/no_deploy_iwmailheader.tpl
        
        This is the Mail Header presentation template.  It is used to produce
        the header of an email message that reports a problem deploying files.
        This file parsed by 
	the custom tag iwov_map module and merged with the task output XML file.
	After parsed by the custom tag iwov_map module and merged with the task output
	XML file, the file will follow the DTD header_xml.dtd under 
	iw-home/local/config/wft/solutions directory.

	If you want to check the well-formedness of this XML file, you can use 
	the tool: iw-home/private/bin/iwxmlval <path-to-the-xml-file>
-->

<headers>
    <!-- To the owner of the Resolve Deploy task -->
    <to>
        <iwov_emailmap user='{iw_value name="dcr.workflowinformation.task[1]@owner"/}' />
        <!-- You can also hard-wire addresses.  For example: -->
        <!-- <iwov_emailmap user='jsmith' /> -->
    </to>
    <!-- You can CC additional addresses.  For example: -->
    <cc>
        <!-- <iwov_emailmap user='jane.doe' /> -->
    </cc>
    <!-- From the owner of the email task -->
    <from>
        <iwov_emailmap user='{iw_value name="dcr.workflowinformation.task[0]@owner"/}' />
    </from>
    <!-- Use the description of the Resolve Deploy task as the subject -->
    <subject>
        <iw_value name='dcr.workflowinformation.task[1].description'/>
    </subject>
</headers>
