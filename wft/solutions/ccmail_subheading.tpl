<iw_comment>
----------------------------------------------------------------------
ccmail_subheader.tpl

Create an HTML subheading with the label provided.

This templates attempts to emulate the format of a heading widget
with JSP="/base/widget/heading/heading_renderer_sub.jsp".  It is
used by ccmail_htmlbody.tpl.

ARGUMENTS:
    title - The title to use in the subheading
    actions - (optional) HTML for actions list
----------------------------------------------------------------------
</iw_comment>
<iw_perl>
<![CDATA[
	 my $title = $iw_arg{title};
         my $actions = $iw_arg{actions} || '&nbsp;';
]]>
</iw_perl>
<table border="0" width="100%" height="25" cellspacing="0" cellpadding="0">
  <tr>
    <td class="widget_heading_optionbartitle" nowrap><span class="iw-base-heading-title"><iw_value name="$title"/></span>
    </td>
    <td align="right">
      <table border="0" cellspacing="0" cellpadding="2" align="right">
        <tr>
          <td>
            <iw_value name="$actions"/>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="3" class="iw-base-horizontal-divider"><img src="/iw-cc/base/images/clear.gif" width="1" height="1"></td>
  </tr>
</table>
