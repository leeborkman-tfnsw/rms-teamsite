<iw_comment>
----------------------------------------------------------------------
ccmail_button.tpl

Create an HTML button.

This templates attempts to emulate the format of a button widget.

ARGUMENTS:
    label - The label to use on the button
    href - The URL activated by the button
----------------------------------------------------------------------
</iw_comment>
<iw_perl>
<![CDATA[
	 my $label = $iw_arg{label};
	 my $href = $iw_arg{href};
]]>
</iw_perl>
<table cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td><img src="/iw-cc/base/images/dialog_btn_left.gif"/></td>
    <td nowrap valign="middle" align="middle" background="/iw-cc/base/images/dialog_btn_mid.gif">
      <a href="<iw_value name="$href" />"
         class="iw-base-link"><iw_value name="$label" /></a>
    </td>
    <td><img src="/iw-cc/base/images/dialog_btn_right.gif"/></td>
  </tr>
</table>
